<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ZoneCategory extends Model
{
    protected $table = 'zone_categories';
}

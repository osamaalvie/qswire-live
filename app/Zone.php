<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class Zone extends Model
{
    protected $table = 'zones';

    public function stats()
    {
        return $this->hasMany(Stat::class, 'type_id')->where('stat_type', Config::get('constants.stat_types.zone'));
    }

    public function followings()
    {
        return $this->stats()->where('action_type', '=', Config::get('constants.action_types.followings'));
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function isUserFollow()
    {

        return $this->followings()->where('user_id', auth()->user()->id)->count() > 0;
    }

    public function questions()
    {
        return $this->belongsToMany(Question::class, 'zone_questions', 'zone_id');
    }

    public function followers()
    {
        return User::query()
            ->select('users.*')
            ->join('stats', 'users.id', '=', 'stats.user_id')
            ->where('type_id', '=', $this->id)
            ->where('user_id', '!=', $this->user_id)
            ->where('stats.stat_type', '=', Config::get('constants.stat_types.zone'))
            ->where('stats.action_type', '=', Config::get('constants.action_types.followings'));

    }
}

<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserActionMail extends Mailable
{
 //   use Queueable, SerializesModels;

    public $message;
    public $user;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $message)
    {
        $this->message = $message;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data['user'] = $this->user;
        $data['message'] = $this->message;
        return $this->from('osamaalvie@gmail.com')
            ->subject('Qswire Notifications')
            ->view('emails.action', compact('data'));
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

class Comment extends Model
{
    protected $table = 'comments';

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function stats()
    {
        return $this->hasMany(Stat::class, 'type_id')->where('stat_type', Config::get('constants.stat_types.comment'));
    }

    public function likes()
    {
        return $this->stats()->where('action_type', '=', Config::get('constants.action_types.likes'));
    }

    public function replies()
    {
        return $this->hasMany(Reply::class, 'type_id');
    }

    public function userLikesCount()
    {
        return $this->likes()->where('user_id', auth()->user()->id)->count();
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;

class Question extends Model
{
    protected $table = 'questions';

    protected $fillable = ['topics'];

    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = Str::substr($value, -1) != '?' ? $value . '?' : $value;
    }


    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function topics()
    {
        return $this->belongsToMany(Topic::class, 'questions_topics', 'question_id', 'topic_id');
    }

    public function answers()
    {
        return $this->hasMany(Answer::class, 'question_id');
    }

    public function getQuestionLinkAttribute()
    {
        return preg_replace('#[ -]+#', '-', strtolower($this->question));

    }

    public function stats()
    {
        return $this->hasMany(Stat::class, 'type_id')->where('stat_type', Config::get('constants.stat_types.question'));
    }

    public function views()
    {
        return $this->stats()->where('action_type', '=', Config::get('constants.action_types.views'));
    }

    public function likes()
    {
        return $this->stats()->where('action_type', '=', Config::get('constants.action_types.likes'));
    }

    public function comments()
    {
        return $this->hasMany(Comment::class, 'type_id');
    }

    public function followings()
    {
        return $this->stats()->where('action_type', '=', Config::get('constants.action_types.followings'));

    }

    public function downvotes()
    {
        return $this->stats()->where('action_type', '=', Config::get('constants.action_types.downvote'));

    }

    public function zone()
    {
        return $this->belongsToMany(Zone::class, 'zone_questions', 'question_id', 'zone_id');

    }

    public function userFollow()
    {
        return $this->followings()->where('user_id', auth()->user()->id);

    }


    public function isUserFollow()
    {
        return $this->userFollow()->count() > 0;
    }



}

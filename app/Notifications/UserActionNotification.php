<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Auth;

class UserActionNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public $message;
    public $user;

    /**
     * Create a new notification instance.
     * UserActionNotification constructor.
     * @param $user
     * @param $message
     */
    public function __construct($user, $message)
    {
        $this->message = $message;
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'message' => $this->message
        ];
    }

    public function toMail($notifiable)
    {
        $data['user'] = $this->user;
        $data['message'] = $this->message;

        return (new MailMessage)->view('emails.action', compact('data'));
    }

//    public function toBroadcast($notifiable)
//    {
//        return new BroadcastMessage([
//            'message' => "$this->message (User $notifiable->id)"
//        ]);
//    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

class Answer extends Model
{
    protected $table = 'answers';

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function question()
    {
        return $this->hasOne(Question::class, 'id', 'question_id');
    }

    public function stats()
    {
        return $this->hasMany(Stat::class, 'type_id')->where('stat_type', Config::get('constants.stat_types.answer'));
    }

    public function views()
    {
        return $this->stats()->where('action_type', '=', Config::get('constants.action_types.views'));
    }

    public function likes()
    {
        return $this->stats()->where('action_type', '=', Config::get('constants.action_types.likes'));
    }

    public function comments()
    {
        return $this
            ->hasMany(Comment::class, 'type_id')
            ->where('stat_type', Config::get('constants.stat_types.answer'));

    }

    public function userLikesCount()
    {
        return $this->likes()->where('user_id', auth()->user()->id)->count();
    }


}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';


    public function zone()
    {
        return $this->belongsToMany(Zone::class, 'zone_posts', 'post_id', 'zone_id');

    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
    /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $user = Auth::user();

        $notifications = $user->notifications()->orderByDesc('created_at')->limit(10)->get();

        foreach ($user->unreadNotifications as $notification) {
            $notification->markAsRead();
        }

        return view('notifications.index', compact('notifications'));

    }
}

<?php

namespace App\Http\Controllers;

use App\Events\UserActionEvent;
use App\Mail\UserActionMail;
use App\Question;
use App\Stat;
use App\Zone;
use App\ZoneCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use SebastianBergmann\CodeCoverage\Report\PHP;

class ZoneController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param string $question
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $categories = ZoneCategory::query()->pluck('name', 'id');

        $userZones = Zone::query()->get();

        $uniqueCategories = $userZones->unique('category_id');

        $zoneGroups = [];

        if ($uniqueCategories) {

            foreach ($uniqueCategories as $k => $category) {

                $zoneGroups[] = [
                    'name' => $categories[$category->category_id],
                    'zones' => $userZones->where('category_id', $category->category_id),
                ];

            }

        }


        return view('zones.index', compact('categories', 'zoneGroups'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:zones',
            'category' => 'required|max:50',
            'description' => 'required|max:90',
            'keywords' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator->errors());
        }

        try {
            DB::beginTransaction();

            $zone = new Zone();
            $zone->name = $request->get('name');
            $zone->slug = Str::slug($request->get('name'));
            $zone->category_id = $request->get('category');
            $zone->description = $request->get('description');
            $zone->keywords = $request->get('keywords');
            $zone->user_id = auth()->user()->id;
            $zone->save();

            $stats = new Stat();
            $stats->type_id = $zone->id;
            $stats->user_id = Auth::user()->id;
            $stats->stat_type = Config::get('constants.stat_types.zone');
            $stats->action_type = Config::get('constants.action_types.followings');
            $stats->save();

            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            return Redirect::back()->withErrors(['errors' => $e->getMessage()]);

        }


        return Redirect::back()->with('success', 'Zone has been created successfully!');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function lists()
    {

        $zones = Auth::user()->zones()->paginate(10);

        try {
            $view = view('zones.partials.listing', compact('zones'))->render();
        } catch (\Throwable $e) {
            $view = '<span>' . $e->getMessage() . '</span>';
        }

        return response()->json(['view' => $view, 'count' => $zones->count()], 200);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function followZone(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'zone_id' => 'required',
            'user_id' => 'required',
            'following' => 'required',
        ]);

        if ($validator->fails()) {

            if ($request->ajax()) {
                return response()->json(['status' => false, 'message' => $validator->errors()], 500);
            }

            return Redirect::back()->withErrors($validator->errors());
        }

        $userID = $request->get('user_id');
        $zoneID = $request->get('zone_id');
        $following = $request->get('following');

        $zone = Zone::query()->find($zoneID);

        $userFollowing = $zone->followings()->where('user_id', Auth::user()->id);
        $message = 'followZone';

        try {

            if ($following == 'false') {
                $userFollowing->delete();
                //$message = 'unFollowZone';
            } else {

                $stats = new Stat();
                $stats->type_id = $zoneID;
                $stats->user_id = Auth::user()->id;
                $stats->stat_type = Config::get('constants.stat_types.zone');
                $stats->action_type = Config::get('constants.action_types.followings');
                $stats->save();

                if ($userID != Auth::user()->id) {

                    $message = str_replace('{userName}', Auth::user()->fullName(), Config::get("constants.user_actions_messages.{$message}"));

                    $user = $zone->user;
                    $user->notify(new \App\Notifications\UserActionNotification($user, $message));
                    event(new UserActionEvent($zone->user->id, $message));

                    try{
                        Mail::to($user)
                            ->send(new UserActionMail($user, $message));
                    }catch (\Exception $e){

                    }

                }
            }



            if ($request->ajax()) {
                return response()->json(['status' => true, 'message' => 'success', 'data' => ['count' => $zone->followings()->count(), 'following' => $userFollowing->count()]], 200);
            }

            return Redirect::back();


        } catch (\Exception $e) {

            if ($request->ajax()) {

                return response()->json(['status' => false, 'message' => $e->getMessage()], 500);

            }

            return Redirect::back();

        }

    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view($slug, Request $request)
    {
        $zone = Zone::query()->with(['questions' => function ($query) {
            $query->orderByDesc('id')->limit(5);
        }, 'questions.userFollow'])->where('slug', $slug)->first();

        return view('zones.view_zone', compact('zone'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeQuestion(Request $request)
    {
        $validatedData = $request->validate([
            'question' => 'required|max:255',
            'zone_id' => 'required',
        ]);

        $hashedQuestion = getHash(Str::lower($request->get('question')));

        if (Question::query()->where('q_key', $hashedQuestion)->exists()) {
            return Redirect::back()->with('error', 'Question already exist');
        }

        try {

            DB::beginTransaction();

            $question = new Question();
            $question->user_id = Auth::user()->id;
            $question->title = $validatedData['question'];
            $question->slug = Str::slug($validatedData['question']);
            $question->q_key = $hashedQuestion;
            $question->save();
            $question->zone()->sync($validatedData['zone_id']);

            $stats = new Stat();
            $stats->type_id = $question->id;
            $stats->user_id = Auth::user()->id;
            $stats->stat_type = Config::get('constants.stat_types.question');
            $stats->action_type = Config::get('constants.action_types.followings');
            $stats->save();

            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            return Redirect::back()->with('error', $e->getMessage());
        }

        return Redirect::back()->with('success', 'Question has been created successfully!');

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function followQuestion(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'question_id' => 'required',
            'zone_id' => 'required',
            'user_id' => 'required',
            'following' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => $validator->errors()], 500);
        }

        $userID = $request->get('user_id');
        $zoneID = $request->get('zone_id');
        $questionID = $request->get('question_id');
        $following = $request->get('following');

        $zone = Zone::query()->find($zoneID);
        $question = Question::query()->find($questionID);

        $userFollowing = $question->followings()->where('user_id', Auth::user()->id);

        try {

            if ($following == 'false') {
                $userFollowing->delete();
            } else {
                $stats = new Stat();
                $stats->type_id = $questionID;
                $stats->user_id = Auth::user()->id;
                $stats->stat_type = Config::get('constants.stat_types.question');
                $stats->action_type = Config::get('constants.action_types.followings');
                $stats->save();

                if ($userID != Auth::user()->id) {
                    $user = Auth::user();

                    $message = str_replace('{userName}', $user->fullName(), Config::get('constants.user_actions_messages.followQuestion'));

                    $zone->user->notify(new \App\Notifications\UserActionNotification($user,$message));
                    event(new UserActionEvent($zone->user->id, $message));
                }
            }

            return response()->json(['status' => true, 'message' => 'success', 'data' => ['count' => $question->followings()->count(), 'following' => $userFollowing->count()]], 200);


        } catch (\Exception $e) {

            return response()->json(['status' => false, 'message' => $e->getMessage()], 500);

        }
    }


    /**
     * @param $zone
     * @param $question
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewQuestion($zone, $question, Request $request)
    {

        $zone = Zone::query()->where('slug', $zone)->first();
        $question = Question::query()->where('slug', $question)->first();
        return view('zones.view_question', compact('zone', 'question'));
    }

    /**
     * @param $slug
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function membersView($slug, Request $request)
    {
        $zone = Zone::query()->where('slug', $slug)->first();

        return view('zones.members', compact('zone'));
    }

    /**
     * @param $slug
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function questionsView($slug, Request $request)
    {
        $zone = Zone::query()->where('slug', $slug)->first();

        return view('zones.questions', compact('zone'));

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function questions(Request $request)
    {
        $id = $request->get('zone_id');
        $zone = Zone::query()->find($id);

        $questions = $zone->questions()->paginate(2);

        try {
            $view = view('zones.partials.question', compact('zone', 'questions'))->render();
        } catch (\Throwable $e) {
            $view = '<span>' . $e->getMessage() . '</span>';
            return response()->json(['view' => $view, 'message' => $e->getMessage()], 500);

        }

        return response()->json(['view' => $view, 'count' => $questions->count()], 200);
    }
}

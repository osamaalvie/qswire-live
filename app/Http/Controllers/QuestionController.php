<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Comment;
use App\Events\UserActionEvent;
use App\Reply;
use App\User;
use Illuminate\Http\Request;
use App\Question;
use App\Stat;
use App\Topic;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Pusher\Pusher;
use Pusher\PusherException;

class QuestionController extends Controller
{
    /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param string $question
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $auth = Auth::user();

        if (!$auth->topics()->count()) {
            $topics = Topic::query()->get()->pluck('title', 'id');

            return view('pre_home', compact('topics'));
        }

        return view('home');
    }

    /**
     * @param string $question
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function replica()
    {
        if (!Auth::user()->topics()->count()) {
            return view('pre_home');
        }

        $question = Question::query()->find(69);

        return view('replica', compact('question'));
    }

    /**
     * @param $question
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function findQuestionByName($question, Request $request)
    {
        $question = Question::query()
            ->select('questions.*')
            ->with('topics', 'user', 'views', 'answers', 'answers.user')
            ->where('slug', trim($question))
            ->first();

        $user = auth()->user();
        $userTopicIds = $user->topics()->pluck('topic_id');
        $userTopicIds = array_flip($userTopicIds->toArray());

        if (!isset($userTopicIds[$question->topics->first()->pivot->topic_id])) {

            return abort(404);
        }


        //dd($question->topics->first()->pivot->topic_id);

        $answer = $question->answers()->where('user_id', $user->id)->first();

        $count = $question->views()->where('user_id', $user->id)->count();

        $userQuestionFollow = $question->followings()->where('user_id', $user->id)->count();
        $userQuestionDownVote = $question->downvotes()->where('user_id', $user->id)->count();

        if ($question->user->id != $user->id & !$count) {

            $stats = new Stat();
            $stats->type_id = $question->id;
            $stats->user_id = $user->id;
            $stats->action_type = Config::get('constants.action_types.views');
            $stats->stat_type = Config::get('constants.stat_types.question');
            $stats->save();

        }

        if (!$question) {
            abort(404);
        }

        return view('questions.view_question', compact('question', 'user', 'answer', 'userQuestionFollow', 'userQuestionDownVote'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'question' => 'required|max:255',
            'topics' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->with('error', $validator->errors()->first());
        }

        $hashedQuestion = getHash(Str::lower($request->get('question')));

        if (Question::query()->where('q_key', $hashedQuestion)->exists()) {
            return Redirect::back()->with('error', 'Question already exist');
        }

        try {
            DB::beginTransaction();

            $question = new Question();
            $question->user_id = Auth::user()->id;
            $question->title = $request->get('question');
            $question->slug = Str::slug($request->get('question'));
            $question->q_key = $hashedQuestion;
            $question->save();
            $question->topics()->sync($request->get('topics'));

            $stats = new Stat();
            $stats->type_id = $question->id;
            $stats->user_id = Auth::user()->id;
            $stats->stat_type = Config::get('constants.stat_types.question');
            $stats->action_type = Config::get('constants.action_types.followings');
            $stats->save();

            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            return Redirect::back()->with('error', $e->getMessage());
        }

        return Redirect::back()->with('success', 'Question created successfully!');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {
        try {

            $questionID = $request->get('question_id');

            $question = Question::query()->find($questionID);

            if ($question->answers()->count()) {
                return response()->json(['status' => false, 'message' => 'This question has answers.'], 200);
            }

            if ($question->user->id != auth()->user()->id) {
                return response()->json(['status' => false, 'message' => 'Permission denied'], 403);
            }


            DB::beginTransaction();

            $question->topics()->sync([]);
            $question->delete();

            DB::commit();

            return response()->json(['status' => true], 200);

        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['status' => false, 'message' => $e->getMessage()], 500);

        }

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeTopics(Request $request)
    {

        $validatedData = $request->validate([
            'topics' => 'required',
        ]);

        try {
            Auth::user()->topics()->sync($validatedData['topics']);
            return redirect()->route('home');

        } catch (\Exception $e) {
            return Redirect::back()->with('error', $e->getMessage());
        }
    }


    /**
     * get all questions under user topics
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function all()
    {
        $questions = Question::query()
            ->with('user')
            ->select('questions.*')
            ->join('questions_topics', 'questions.id', '=', 'questions_topics.question_id')
            ->whereIn('questions_topics.topic_id', Auth::user()->topics()->pluck('id'))
            ->orderByDesc('questions.created_at')
            ->groupBy('questions.id')
            ->limit(10)
            ->get();

        return view('questions.index', compact('questions'));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function pusherMessages()
    {

        try {
            $notifications = Auth::user()->unreadNotifications->count();
            return response()->json(['status' => true, 'count' => $notifications], 200);

        } catch (\Exception $e) {

            return response()->json(['status' => false, 'message' => $e->getMessage()], 500);

        }


    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeAnswer(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'answer' => 'required',
                'question_id' => 'required',
            ]);

            if ($validator->fails()) {
                return Redirect::back()->withErrors($validator);
            }

            $answer = new Answer();

            if ($request->has('answer_id')) {
                $answer = Answer::query()->find($request->get('answer_id'));
            }


            $answer->content = $request->get('answer');
            $answer->question_id = $request->get('question_id');
            $answer->user_id = Auth::user()->id;
            $answer->save();


            if ($answer->question->user->id != Auth::user()->id) {
                $user = $answer->question->user;
                $message = str_replace('{userName}', Auth::user()->fullName(), Config::get("constants.user_actions_messages.answer"));

                $user->notify(new \App\Notifications\UserActionNotification($user, $message));
                event(new UserActionEvent($user->id, $message));
            }

            return Redirect::back();

        } catch (\Exception $e) {
            return Redirect::back()->withErrors(['errors' => $e->getMessage()]);

        }

    }

    /**
     *************************************************************************************
     *                                      AJAX CALLS
     *************************************************************************************
     */

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function questions(Request $request)
    {
        $questions = Question::query()
            ->select('questions.*')
            ->with('user', 'answers', 'likes', 'comments')
            ->join('questions_topics', 'questions.id', '=', 'questions_topics.question_id')
            ->join('answers', 'questions.id', '=', 'answers.question_id')
            ->whereIn('questions_topics.topic_id', Auth::user()->topics()->pluck('id'))
            ->orderByDesc('questions.created_at')
            ->groupBy('questions.id')
            ->paginate(10);

        $notifications = Auth::user()->unreadNotifications;

        try {
            $view = view('questions.partials.listing', compact('questions'))->render();
        } catch (\Throwable $e) {
            $view = '<span>' . $e->getMessage() . '</span>';
        }

        return response()->json(['view' => $view, 'notifications' => $notifications, 'count' => $questions->count()], 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function like(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'type_id' => 'required',
            'user_id' => 'required',
            'type' => 'required',
            'is_like' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => $validator->errors()], 500);
        }


        $type = $request->get('type');
        $typeID = $request->get('type_id');
        $userID = $request->get('user_id');
        $isLike = $request->get('is_like');

        $modal = 'App\Answer';


        if ($type == 'comment') {
            $modal = 'App\Comment';
        }

        if ($type == 'reply') {
            $modal = 'App\Reply';
        }

        try {

            $model = $modal::query()->find($typeID);
            $userLikes = $model->likes()->where('user_id', Auth::user()->id);

            if ($isLike === 'false') {
                $userLikes->delete();

            } else {

                if ($userLikes->count()) {
                    exit;
                }

                $stats = new Stat();
                $stats->type_id = $typeID;
                $stats->user_id = Auth::user()->id;
                $stats->stat_type = Config::get("constants.stat_types.$type");
                $stats->action_type = Config::get('constants.action_types.likes');
                $stats->save();

                if ($userID != Auth::user()->id) {
                    $user = User::query()->find($userID);
                    $messageType = $isLike === 'false' ? 'unlike' : 'like';
                    $message = str_replace('{userName}', Auth::user()->fullName(), Config::get("constants.user_actions_messages.like"));
                    $message = str_replace('{Action}', $messageType, $message);
                    $message = str_replace('{Resource}', $type, $message);

                    $user->notify(new \App\Notifications\UserActionNotification($user, $message));
                    event(new UserActionEvent($user->id, $message));
                }
            }


            return response()->json(['status' => true, 'message' => 'success', 'data' => ['count' => $model->likes()->count(), 'userLikeCount' => $model->userLikesCount()]], 200);

        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage()], 500);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function followQuestion(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'question_id' => 'required',
            'user_id' => 'required',
            'following' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => $validator->errors()], 500);
        }

        $userID = $request->get('user_id');
        $questionID = $request->get('question_id');
        $following = $request->get('following');


        $question = Question::query()->find($questionID);
        $userFollowing = $question->followings()->where('user_id', Auth::user()->id);

        try {

            if ($following == 'false') {
                $userFollowing->delete();
            } else {
                $stats = new Stat();
                $stats->type_id = $questionID;
                $stats->user_id = Auth::user()->id;
                $stats->stat_type = Config::get('constants.stat_types.question');
                $stats->action_type = Config::get('constants.action_types.followings');
                $stats->save();

                if ($userID != Auth::user()->id) {
                    $user = $question->user;
                    $message = str_replace('{userName}', Auth::user()->fullName(), Config::get('constants.user_actions_messages.followQuestion'));
                    $user->notify(new \App\Notifications\UserActionNotification($user, $message));
                    event(new UserActionEvent($user->id, $message));
                }
            }


            return response()->json(['status' => true, 'message' => 'success', 'data' => ['count' => $question->followings()->count(), 'following' => $userFollowing->count()]], 200);

        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage()], 500);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function comment(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'comment' => 'required|max:255',
                'answer_id' => 'required',
            ]);

            if ($validator->fails()) {
                if ($request->ajax()) {
                    return response()->json(['status' => false, 'message' => $validator], 500);
                }

                return Redirect::back();
            }

            $comment = new Comment();
            $comment->content = $request->get('comment');
            $comment->type_id = $request->get('answer_id');
            $comment->user_id = Auth::user()->id;
            $comment->stat_type = Config::get('constants.stat_types.answer');
            $comment->save();

            $answer = Answer::query()->find($comment->type_id);
            $user = $answer->user;

            $view = view('questions.partials.comment', compact('comment', 'answer', 'user'))->render();
            $count = $answer->comments->count();

            if ($answer->user->id != Auth::user()->id) {
                $user = $answer->user;
                $message = str_replace('{userName}', Auth::user()->fullName(), Config::get('constants.user_actions_messages.comment'));
                $user->notify(new \App\Notifications\UserActionNotification($user, $message));
                event(new UserActionEvent($answer->user->id, $message));
            }

            if ($request->ajax()) {
                return response()->json(['status' => true, 'message' => 'success', 'data' => ['count' => $count, 'html' => $view]], 200);
            }

            return Redirect::back()->with('success', 'Comment created successfully!');


        } catch (\Exception $e) {

            if ($request->ajax()) {
                return response()->json(['status' => false, 'message' => $e->getMessage()], 500);
            }

            return Redirect::back()->withErrors(['errors' => $e->getMessage()]);


        } catch (\Throwable $e) {

            if ($request->ajax()) {
                return response()->json(['status' => false, 'message' => $e->getMessage()], 500);
            }

            return Redirect::back()->withErrors(['errors' => $e->getMessage()]);


        }


    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAnswers(Request $request)
    {
        $validator = Validator::make($request->all(), [

            'question_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => $validator->errors()], 500);
        }
        $questionID = $request->get('question_id');
        $question = Question::query()->find($questionID);

        $answers = $question->answers()->orderByDesc('created_at')->paginate(2);

        try {

            $view = view('questions.partials.answers', compact('answers'))->render();
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage()], 500);

        } catch (\Throwable $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage()], 500);
        }

        return response()->json(['status' => true, 'view' => $view, 'count' => $answers->count()], 200);


    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function downVoteQuestion(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'question_id' => 'required',
            'user_id' => 'required',
            'down_vote' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => $validator->errors()], 500);
        }

        $userID = $request->get('user_id');
        $questionID = $request->get('question_id');
        $downVote = $request->get('down_vote');

        $question = Question::query()->find($questionID);
        $userDownVote = $question->downvotes()->where('user_id', Auth::user()->id);

        try {

            if ($downVote == 'false') {
                $userDownVote->delete();
            } else {
                $stats = new Stat();
                $stats->type_id = $questionID;
                $stats->user_id = Auth::user()->id;
                $stats->stat_type = Config::get('constants.stat_types.question');
                $stats->action_type = Config::get('constants.action_types.downvote');
                $stats->save();

                if ($userID != Auth::user()->id) {
                    $user = $question->user;
                    $message = str_replace('{userName}', Auth::user()->fullName(), Config::get('constants.user_actions_messages.downvote'));

                    $user->notify(new \App\Notifications\UserActionNotification($user, $message));
                    event(new UserActionEvent($question->user->id, $message));
                }
            }


            return response()->json(['status' => true, 'message' => 'success', 'data' => ['count' => $question->downvotes()->count(), 'userDownVote' => $userDownVote->count()]], 200);

        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage()], 500);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function commentReply(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'reply' => 'required|max:255',
                'comment_id' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => false, 'message' => $validator], 500);
            }

            $reply = new Reply();
            $reply->content = $request->get('reply');
            $reply->type_id = $request->get('comment_id');
            $reply->user_id = Auth::user()->id;
            $reply->stat_type = Config::get('constants.stat_types.comment');
            $reply->save();

            $comment = Comment::query()->find($reply->type_id);
            $user = $comment->user;

            $view = view('questions.partials.replies', compact('reply', 'comment', 'user'))->render();
            $count = $comment->replies()->count();

            if ($comment->user->id != Auth::user()->id) {
                $user = $comment->user;
                $message = str_replace('{userName}', Auth::user()->fullName(), Config::get('constants.user_actions_messages.replies'));
                $user->notify(new \App\Notifications\UserActionNotification($user, $message));
                event(new UserActionEvent($comment->user->id, $message));
            }

            return response()->json(['status' => true, 'message' => 'success', 'data' => ['count' => $count, 'html' => $view]], 200);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage()], 500);

        } catch (\Throwable $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage()], 500);

        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class PostController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'zone_id' => 'required',
            'title' => 'required',
            'post' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->with('error', $validator->errors()->first());
        }

        $hashedPost = getHash($request->get('title'));

        if (Post::query()->where('key', $hashedPost)->exists()) {
            return Redirect::back()->with('error', 'Post already exist');
        }

        try {
            DB::beginTransaction();

            $post = new Post();
            $post->title = $request->get('title');
            $post->slug = Str::slug($request->get('title'));
            $post->content = $request->get('post');
            $post->key = $hashedPost;
            $post->user_id = auth()->user()->id;
            $post->save();

            $post->zone()->sync($request->get('zone_id'));

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            return Redirect::back()->with('error', $exception->getMessage());
        }

        return Redirect::back()->with('success', 'Post has been created successfully');
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: Dell
 * Date: 8/16/2021
 * Time: 2:36 PM
 */

namespace App\Http\View\Composers;


use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class AuthComposer
{

    /**
     * Create a new profile composer.
     *
     * @param Auth $user
     * @return void
     */
    public function __construct()
    {
        // Dependencies automatically resolved by service container...

    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('auth',  Auth::user());
    }
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stat extends Model
{
    protected $table = 'stats';
    protected $fillable = ['question_id','stat_type', 'created_at', 'updated_at'];


}

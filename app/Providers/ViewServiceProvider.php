<?php

namespace App\Providers;

use App\Http\View\Composers\AuthComposer;
use App\Topic;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('questions.modals.askQuestionModal', function ($view) {
            $topics = Topic::query()->get()->pluck('title', 'id');
            $view->with('topics', $topics);
        });


        View::composer('*', AuthComposer::class);

        View::composer('zones.user_zones', function ($view) {
            $zones = auth()->user()->zones()->orderByDesc('id')->limit(5)->get();
            $view->with('userZones', $zones);
        });


    }
}

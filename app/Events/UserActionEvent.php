<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class UserActionEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $userID;

    public $message;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($userID, $message)
    {
        $this->userID = $userID;
        $this->message = $message;
    }

    public function broadcastOn()
    {
        return ['user-actions-' . $this->userID];
    }


    public function broadcastAs()
    {

        return 'user-action-event';

    }
}

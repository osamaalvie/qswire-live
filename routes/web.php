<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes(['verify' => true]);

Route::group(['middleware' => ['web', 'verified']], function () {
    Route::get('/', 'QuestionController@index')->name('home');
    Route::get('/replica', 'QuestionController@replica')->name('replica');
    Route::get('/user/{name}', 'UserController@profile')->name('user.profile');
    Route::get('/questions', 'QuestionController@all')->name('question.all');
    Route::get('/pusherMessages', 'QuestionController@pusherMessages')->name('question.pusherMessages');
    Route::post('/questions', 'QuestionController@questions')->name('questions');
    Route::post('/questions/destroy', 'QuestionController@destroy')->name('question.destroy');
    Route::post('/store', 'QuestionController@store')->name('question.store');
    Route::post('/topics', 'QuestionController@storeTopics')->name('user.topics');
    Route::post('/questionLike', 'QuestionController@likeQuestion')->name('question.like');
    Route::post('/answer/like', 'QuestionController@likeAnswer')->name('answer.like');
    Route::post('/action/like', 'QuestionController@like')->name('action.like');
    Route::post('/comment/like', 'QuestionController@likeComment')->name('comment.like');
    Route::post('/comment', 'QuestionController@comment')->name('answer.comment');
    Route::post('/comment/reply', 'QuestionController@commentReply')->name('comment.reply');
    Route::post('/answer', 'QuestionController@storeAnswer')->name('answer.store');
    Route::post('/answers', 'QuestionController@getAnswers')->name('question.answers');
    Route::post('/followQuestion', 'QuestionController@followQuestion')->name('question.follow');
    Route::post('/downVoteQuestion', 'QuestionController@downVoteQuestion')->name('question.downvote');
    Route::post('/z/{zone}/post/store', 'PostController@store')->name('zone.post.store');

    Route::get('/notifications', 'NotificationController@index')->name('notifications.index');


    //ZONES
    Route::get('/zones', 'ZoneController@index')->name('zones.index');
    Route::post('/zones/list', 'ZoneController@lists')->name('zones.list');
    Route::post('/zones/store', 'ZoneController@store')->name('zone.store');
    Route::post('/zones/follow', 'ZoneController@followZone')->name('zone.follow');
    Route::post('/zones/question/store', 'ZoneController@storeQuestion')->name('zone.question.store');
    Route::post('/zones/question/follow', 'ZoneController@followQuestion')->name('zone.question.follow');

    Route::get('/z/{name}', 'ZoneController@view')->name('zone');
    Route::get('/z/{name}/members', 'ZoneController@membersView')->name('zone.members');
    Route::get('/z/{name}/questions', 'ZoneController@questionsView')->name('zone.questions');
    Route::post('/z/{zone}/fetchQuestions', 'ZoneController@questions')->name('zone.fetchQuestions');
    Route::get('/z/{zone}/{question}', 'ZoneController@viewQuestion')->name('zone.question');



    Route::get('/{question}', 'QuestionController@findQuestionByName')->name('question.select');


});


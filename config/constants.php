<?php
/**
 * Created by PhpStorm.
 * User: Dell
 * Date: 7/20/2021
 * Time: 9:38 PM
 */

return [
    'stat_types' => [
        'question' => 1,
        'answer' => 2,
        'comment' => 3,
        'zone' => 4,
        'reply' => 5,
    ],
    'action_types' => [
        'views' => 1,
        'likes' => 2,
        'comments' => 3,
        'followings' => 4,
        'downvote' => 5,
        'replies' => 6
    ],
    'user_actions_messages' => [
        'like' => '{userName} {Action} your {Resource}',
        'comment' => '{userName} commented on your answer',
        'followQuestion' => '{userName} start following your Question',
        'downvote' => '{userName} dislike your question',
        'replies' => '{userName} reply on your comment',
        'answer' => '{userName} answered on your question',
        'followZone' => '{userName} start following your zone',
        'unFollowZone' => '{userName} unfollow your zone',
    ]
];
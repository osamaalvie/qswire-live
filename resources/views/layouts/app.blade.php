<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <link rel="icon" href="{{ URL::asset('/images/logo.png') }}" type="image/x-icon"/>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <link href="{{ asset('css/toastr.css') }}" rel="stylesheet">


    <style>
        .top-bar i {
            margin-right: 5px;
            font-size: 20px;
            float: left;
        }

        .top-bar .nav-link {
            font-size: 14px;
            color: rgb(96, 96, 96) !important;
            font-weight: 700 !important;
        }

        .top-bar .nav-link.active {
            color: #DC3545 !important;
            border-bottom: 2px solid #DC3545;
            font-weight: 600 !important;
            padding-bottom: .6rem;
        }

        .top-bar .nav-link.active:hover {
            color: #DC3545 !important;

        }

        .top-bar .nav-link:hover {
            color: black !important;

        }

        .modal-open .container-fluid, .modal-open .container {
            -webkit-filter: blur(5px) grayscale(80%);
        }

        .pushNotifications {

            position: relative;
        }

        .notification-count {
            top: -10px;
            left: 10px;
            position: absolute;
        }

        .vertical-center {
            margin: 0;
            position: relative;

            -ms-transform: translateY(25%);
            transform: translateY(25%);
        }

        #profile-menu .profile-link a {
            text-decoration: none;
            color: black;
        }

        .dropdown-menu:before {
            position: absolute;
            top: -7px;
            left: 9px;
            display: inline-block;
            border-right: 7px solid transparent;
            border-bottom: 7px solid #ccc;
            border-left: 7px solid transparent;
            border-bottom-color: rgba(0, 0, 0, 0.2);
            content: '';
        }

        .dropdown-menu:after {
            position: absolute;
            top: -6px;
            left: 10px;
            display: inline-block;
            border-right: 6px solid transparent;
            border-bottom: 6px solid #ffffff;
            border-left: 6px solid transparent;
            content: '';
        }

        .dropdown-menu {
            padding-bottom: 0;
        }

        .dropdown-footer {
            background-color: #f1f1f1;

        }

        .dropdown-footer a {
            color: #a9a9a9;
            font-weight: 600;
        }

        .dropdown-footer a:after {
            content: " \B7 ";
        }

        .dropdown-footer a:last-child:after {
            content: "";
        }

        .toast-top-right {
            top: 55px;
            position: absolute;;
        }

        .navbar-custom {
            height: auto;
        }

    </style>
    @yield('style')
</head>
<body>
<div id="app">
    @if(\Illuminate\Support\Facades\Auth::check())
        <input type="hidden" id="auth_user_id" name="auth_user_id" value="{{$auth->id}}"/>
    @endif

    <nav class="navbar navbar-expand-md navbar-expand-sm navbar-light bg-white shadow-sm py-0 fixed-top">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                <img src="{{ URL::asset('/images/logo.png') }}" width="40" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse mt-auto" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-3 top-bar">
                    <li class="nav-item">
                        <a class="nav-link {{ Request::is('/') ? 'active' : '' }}" href="{{ url('/') }}"><i
                                    class="fa fa-newspaper-o"></i>Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ Request::is('questions') ? 'active' : '' }}"
                           href="{{ route('question.all') }}"><i class="fa fa-edit"></i>Questions</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ Request::is('zones') ? 'active' : '' }}"
                           href="{{ route('zones.index') }}"><i
                                    class="fa fa-square"></i>Zones</a>
                    </li>


                    <li class="nav-item avatar">
                        <a class="nav-link waves-effect waves-light {{ Request::is('notifications') ? 'active' : '' }}"
                           id="navbarDropdownMenuLink-5" href="{{route('notifications.index')}}">
                            <div class="pushNotifications">
                                <span class="badge badge-danger notification-count" style="display: none">0</span>
                                <i class="fa fa-bell"></i>Notifications
                            </div>

                        </a>

                    </li>
                    <li>
                        <form action="#" method="post" class="ml-5">
                            @csrf

                            <div class="input-group col-md-12">
                                <input class="form-control py-2" type="search" value="search" id="example-search-input">
                                <span class="input-group-append">
                                    <button class="btn btn-outline-secondary" type="button">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>
                    </li>

                </ul>


                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <div class="dropdown">
                            <button class="btn btn-default" type="button" id="menu1" data-toggle="dropdown">
                                <img src="{{ URL::asset('/images/avatar.jpg') }}" height="30" width="30">
                                <span class="caret"></span>
                            </button>

                            <ul class="dropdown-menu mt-3" role="menu" aria-labelledby="menu1" style="min-width: 220px"
                                id="profile-menu">
                                <li role="presentation" class="profile-link">
                                    <a href="{{route('user.profile',[\Illuminate\Support\Str::slug($auth->fullName())])}}">
                                        <div class="pl-3 pb-3">
                                            <img src="{{ URL::asset('/images/avatar.jpg') }}" height="35" width="35">
                                        </div>
                                        <div class="ml-3">
                                            <b>  {{ \Str::upper( $auth->fullName()) }}</b>
                                            <i class="fa fa-arrow-right pull-right mr-3 vertical-center"></i>
                                        </div>
                                    </a>

                                </li>
                                <li role="presentation">
                                    <div class="ml-2">
                                        <a class="dropdown-item" role="menuitem" tabindex="-1" href="#">
                                            <i class="fa fa-envelope"></i>
                                            <span class="ml-1">Messages</span>
                                        </a>
                                    </div>
                                </li>
                                <li role="presentation">
                                    <div class="ml-2">
                                        <a class="dropdown-item" role="menuitem" tabindex="-1" href="#">
                                            <i class="fa fa-star"></i>
                                            <span class="ml-1">Bookmarks</span>
                                        </a>
                                    </div>
                                </li>
                                <li role="presentation">
                                    <div class="ml-2">
                                        <a class="dropdown-item" role="menuitem" tabindex="-1" href="#">
                                            <i class="fa fa-tachometer"></i>
                                            <span class="ml-1">Admin Panel</span>
                                        </a>
                                    </div>
                                </li>
                                <li role="presentation" class="divider"></li>
                                <li role="presentation">
                                    <div class="ml-2">
                                        <a class="dropdown-item" role="menuitem" tabindex="-1" href="#">
                                            <i class="fa fa-cog"></i>
                                            <span class="ml-1">Settings</span>
                                        </a>
                                    </div>
                                </li>
                                <li role="presentation">
                                    <div class="ml-2">
                                        <a class="dropdown-item" role="menuitem" tabindex="-1" href="#">
                                            <i class="fa fa-info-circle"></i>
                                            <span class="ml-1">Help</span>
                                        </a>
                                    </div>
                                </li>
                                <li role="presentation">
                                    <div class="ml-2">
                                        <a class="dropdown-item text-danger" role="menuitem"
                                           href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            <i class="fa fa-sign-out"></i>
                                            <span class="ml-1">{{ __('Logout') }}</span>
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                              class="d-none">
                                            @csrf
                                        </form>
                                    </div>

                                </li>
                                <li role="presentation" class="p-3 dropdown-footer">
                                    <a href="#">About</a>
                                    <a href="#">Terms</a>
                                    <a href="#">Privacy</a>
                                </li>

                            </ul>
                        </div>
                    </li>
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="btn btn-sm btn-danger" href="#" data-toggle="modal"
                           data-target="#questionInputModal">
                            Ask Question
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <main class="py-4" style="margin-top: 50px">
        {{--@include('flash_messages')--}}
        @include('toast_messages')
        @yield('content')
        @include('questions.modals.askQuestionModal')
    </main>
</div>
<script>

    $(document).ready(function () {

        toastr.options = {
            "positionClass": "toast-top-right",
        };

        var pusher = new Pusher('32c6b0aaf5a8e8e97158', {
            cluster: 'ap2'
        });

        let userID = $('#auth_user_id').val();

        // Subscribe to the channel we specified in our Laravel Event
        var channel = pusher.subscribe('user-actions-' + userID);

        // Bind a function to a Event (the full Laravel class)
        channel.bind('user-action-event', function (data) {
            toastr.success(data.message);

            retrieveHistory();
        });

        if ($('#showToastMessage').length) {
            // channel.trigger('user-action-event', {'message': 'Question has been created successfully'});
            let input = $('#showToastMessage');
            let type = input.data('type');
            let message = input.data('message');
            toastr[type](message);
        }
        retrieveHistory();

    });

    // channel.bind('pusher:subscription_succeeded', retrieveHistory);

    function retrieveHistory() {
        $.get('{{route('question.pusherMessages')}}').done(function (response) {
            if (response.status && response.count > 0) {

                $('.notification-count').html(response.count).show();
                $('.notification-list').empty();
            }

        });
    }

</script>
@yield('scripts')
</body>
</html>

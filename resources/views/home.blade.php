@extends('layouts.app')

@section('content')
    <div class="container">
        {{--Ask Question Box and Feeds--}}

        <div class="row justify-content-center">
            <div id="user-zones-list-container" class="col-lg-2 col-md-2 col-sm-3 col-xm-3 m-1 p-1">
                @include('zones.user_zones')
            </div>
            <div class="col-lg-7 col-md-6 col-sm-9 col-xm-9 m-1 p-1 mr-auto">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                <div class="col-12 mb-2">
                    <div class="card mb-0 questionInputBox">

                        <span class="questionInputBox-imgBox">
                            <img class="img-md" src="{{ URL::asset('/images/avatar.jpg') }}"/>
                            <b style="font-size:12px">{{auth()->user()->first_name . ' ' .  auth()->user()->last_name}}</b>
                        </span>

                        <a href="#" data-toggle="modal" data-target="#questionInputModal" class="questionInputBox-link">
                            <h3>What's in your mind, Dev Test</h3>
                        </a>

                    </div>

                    <div id="questionFeedsContainer">


                    </div>

                    @include('loadMore')

                </div>

            </div>
        </div>
    </div>



@endsection
@section('style')
    <link href="{{ asset('css/feeds.css') }}" rel="stylesheet">

    <style>
        body {
            background-color: #ededed !important;
        }

        /**
        ************************************************************************************************************
        Question Post Box
        **************************************************************************************************************
        */

        .questionInputBox {
            width: 100%;
            padding: 10px 20px;
            color: #4e4e4e;
            border-radius: 10px !important;
        }

        .questionInputBox h3 {
            font-size: 18px;
            font-family: sans-serif;
            font-weight: 600;
            padding: 10px 0;
            color: #a9a9a9;
        }

        .questionInputBox img {
            width: 40px;
            height: 40px;
            border-radius: 100%;
        }

        .questionInputBox a {
            text-decoration: none;
            background-color: transparent;
        }

        /**
        ************************************************************************************************************
        MODALS
        **************************************************************************************************************
        */


    </style>

@endsection
@section('scripts')
    <script>
        var page = 1;

        $(window).scroll(function () {
            if ($(window).scrollTop() + $(window).height() >= $(document).height()) {
                page++;
                loadMoreQuestions(page);
            }
        });

        function like(e) {
            let button = $(e);
            button.prop('disabled', true);

            let uid = button.data('uid');
            let tid = button.data('tid');
            let type = button.data('type');
            let isLike = button.data('state') > 0 ? 'false' : 'true';
            let count = button.find('.count').data('count');

            $.ajax({
                url: '{{route('action.like')}}',
                datatype: "json",
                type: "POST",
                data: {user_id: uid, type_id: tid, type: type, is_like: isLike},
                beforeSend: function () {

                    button.find('.icon > i').toggleClass('blue');
                    if (isLike === 'true') {
                        count = count + 1;
                    } else {
                        if (count > 0) {
                            count = count - 1;
                        } else {
                            count = 0;
                        }
                    }

                    button.find('.count').text(count);

                }
            }).done(function (response) {
                if (response.status) {
                    button.data('state', response.data.userLikeCount);
                    button.find('.count').data('count', response.data.count);

                    button.prop('disabled', false);
                }


            }).fail(function (jqXHR, ajaxOptions, thrownError) {
                button.prop('disabled', false);
            });

        }

        function comment(e) {
            let form = $(e);
            let aid = $(form).data('aid');
            let comment = $(form).find('#comment').val();

            $.ajax({
                url: '{{route('answer.comment')}}',
                datatype: "json",
                type: "POST",
                data: {comment: comment, answer_id: aid},
                beforeSend: function () {
                    //button.prop('disabled', true);
                }
            }).done(function (response) {

                if (response.status) {
                    // button.prop('disabled', false);
                    //button.find('.question-likes-count').text(response.data.count);
                    let commentList = $('.comment_list[data-aid="' + aid + '"]');

                    if ($('.comment_list[data-aid="' + aid + '"] > li').length < 3) {
                        commentList.prepend(response.data.html);
                    } else {
                        $('.comment_list[data-aid="' + aid + '"] > li:last').remove();
                        commentList.prepend(response.data.html);
                    }

                    $(form).find('#comment').val('');
                    let actionPanel = $('.post-action[data-aid="' + aid + '"]');
                    actionPanel.find('.comment > .count').html(response.data.count);
                    actionPanel.find('.count').data('count', response.data.count);
                }
            }).fail(function (jqXHR, ajaxOptions, thrownError) {
                //button.prop('disabled', false);
            });
        }

        function reply(e) {
            let form = $(e);
            let cid = $(form).data('cid');
            let reply = $(form).find('.replies').val();

            $.ajax({
                url: '{{route('comment.reply')}}',
                datatype: "json",
                type: "POST",
                data: {reply: reply, comment_id: cid},
                beforeSend: function () {
                    //button.prop('disabled', true);
                }
            })
                .done(function (response) {

                    if (response.status) {
                        // button.prop('disabled', false);
                        //button.find('.question-likes-count').text(response.data.count);
                        let replyList = $('.replies_list[data-cid="' + cid + '"]');

                        if ($('.replies_list[data-cid="' + cid + '"] > li').length < 3) {
                            replyList.prepend(response.data.html);
                        } else {
                            $('.replies_list[data-cid="' + cid + '"] > li:last').remove();
                            replyList.prepend(response.data.html);
                        }


                        $(form).find('#reply').val('');

                        let actionPanel = $('.post-action[data-cid="' + cid + '"]');
                        actionPanel.find('.reply > .count').html(response.data.count);
                        actionPanel.find('.reply > .count').data('count', response.data.count);


                    }
                })
                .fail(function (jqXHR, ajaxOptions, thrownError) {
                    //button.prop('disabled', false);
                });
        }

        function loadMoreQuestions(page) {

            $.ajax({
                url: '{{route('questions')}}',
                datatype: "json",
                type: "POST",
                data: {page: page},
                beforeSend: function () {
                    $('.auto-load').show();
                }
            })
                .done(function (response) {
                    if (response.count == 0) {
                        $('.auto-load').html("We don't have more data to display :(");
                        return;
                    }

                    $('.auto-load').hide();
                    $("#questionFeedsContainer").append(response.view);
                    if (response.notifications.length) {
                        $('.notification-count').html(response.notifications.length).show();
                        $('.notification-list').empty();
                    }


                    $(document).off('submit', '.formComment');
                    $(document).on('submit', '.formComment', function (e) {
                        e.preventDefault();
                        comment(this);
                    });

                    $(document).off('submit', '.formReply');
                    $(document).on('submit', '.formReply', function (e) {
                        e.preventDefault();
                        reply(this);
                    });
                })
                .fail(function (jqXHR, ajaxOptions, thrownError) {
                    console.log('Server error occured');
                });


        }

        $(function () {
            loadMoreQuestions(page);
        });
    </script>
@endsection

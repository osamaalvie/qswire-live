@if ($message = Session::get('success'))
    <input type="hidden" id="showToastMessage" data-message="{{$message}}" data-type="success"/>
@endif

@if ($message = Session::get('error'))
    <input type="hidden" id="showToastMessage" data-message="{{$message}}" data-type="error"/>

@endif

@if ($message = Session::get('warning'))
    <input type="hidden" id="showToastMessage" data-message="{{$message}}" data-type="warning"/>

@endif

@if ($message = Session::get('info'))
    <input type="hidden" id="showToastMessage" data-message="{{$message}}" data-type="info"/>

@endif


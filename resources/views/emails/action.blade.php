<div class="container" style="background-color: #edf2f7; width: 100%; height: 100%;position: relative; padding:10px">
    <div class="header" style="width: 100%;text-align: center; padding:10px">
        <h3 style="color:#3d4852">{{ config('app.name') }}</h3>
    </div>
    <div class="content"
         style="background-color: white; width: 50%;  margin: 0 auto;; padding: 32px; min-height: 200px">
        <b style="color:#3d4852">Hello {{$data['user']->fullName()}}! </b><br/><br/>

        {{$data['message']}} <br/><br/>

        Regards, <br/>
        {{ config('app.name') }}
    </div>
    <div class="footer" style="bottom: 0; position: absolute; text-align: center; height: auto; width: 100%; padding: 5px">
        © {{ date('Y') }} {{ config('app.name') }}. @lang('All rights reserved.')

    </div>
</div>

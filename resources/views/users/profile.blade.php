@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row">

            <div class="col-md-3">

            </div>
            <div class="col-md-7">
                <div class="bg-white p-2 pinned">
                    <i class="fa fa-map-pin"></i> <span> Pinned Answers </span> <i class="fa fa-info-circle"></i>
                </div>
            </div>
            <div class="col-md-2">

            </div>
        </div>
    </div>
@endsection

@section('style')
    <style type="text/css">
        .pinned {
            border: 1px solid #d3d9df;
            border-radius: 10px;
            font-family: sans-serif;
            font-size: 12px;
        }
        .pinned i {
            font-size: small;
            color: #6c757d;
        }
    </style>
@endsection

@section('scripts')
    <script>

    </script>
@endsection


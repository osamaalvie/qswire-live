@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7 col-md-6 col-sm-9 col-xm-9 mt-1 m-1 p-1">

                <div class="col-12 mb-2">
                    <div class="card mb-0 qBox">

                        <div class="q-info">
                            <img class="mdp" src="{{ URL::asset('/images/avatar.jpg') }}"/>
                            <b class="font-size-small">{{$auth->fullName()}}</b>
                        </div>

                        <div class="q-content mt-2">
                            <a href="#" data-toggle="modal" data-target="#questionInputModal"
                               class="questionInputBox-link">
                                <h3>What's in your mind, Dev Test</h3>
                            </a>
                        </div>


                    </div>

                    <div id="questionFeedsContainer">

                        <div class="card mt-2">

                            <div class="card-body">

                                <div class="post-box">

                                    <div class="post-info">

                                        <a href="#"><img class="mdp" src="{{ URL::asset('/images/avatar.jpg') }}"/></a>
                                        <span class="post-owner bold">{{$question->user->first_name . ' ' . $question->user->last_name}},</span>
                                        <span class="post-timestamp">{{Carbon\Carbon::parse($question->created_at)->diffForHumans()}}</span>

                                    </div>

                                    <div class="post-title">

                                    </div>

                                    <div class="post-content">

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                    @include('loadMore')

                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')
    <style type="text/css">
        .qBox {
            padding: 15px;
            border-radius: 10px;
        }

        .card {
            border-radius: 10px;
        }

        .mdp {
            width: 35px;
            height: 35px;
        }

        .font-size-small {
            font-size: small;
        }

        .q-content h3 {
            font-size: 18px;
            font-family: sans-serif;
            font-weight: 600;
            padding: 10px 0;
            color: #a9a9a9;
        }
    </style>
@endsection
@section('scripts')
    <script>

        var page = 1;

        $(window).scroll(function () {
            if ($(window).scrollTop() + $(window).height() >= $(document).height()) {
                page++;
                loadMoreQuestions(page);
            }
        });

        function loadMoreQuestions(page) {
            $.ajax({
                url: '{{route('questions')}}',
                datatype: "json",
                type: "POST",
                data: {page: page},
                beforeSend: function () {
                    $('.auto-load').show();
                }
            }).done(function (response) {
                console.log(response);
                $('.auto-load').hide();
            }).fail(function (jqXHR, ajaxOptions, thrownError) {
                console.log(thrownError);
                $('.auto-load').hide();
            });
        }

        $(document).ready(function () {
            loadMoreQuestions(page);
        });


    </script>
@endsection

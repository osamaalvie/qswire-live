@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row justify-content-center">
            <div class="col-md-8">


                <div class="card">

                    <div class="card-header">
                        <h3>Notifications</h3>

                    </div>

                    <div class="card-body">

                        @forelse($notifications as $notification)

                            <div class="notification-box mt-3">
                                <div class="notification-content-box d-flex">
                                    <div class="notification-profile">
                                        <a href="#"><img src="{{ URL::asset('/images/avatar.jpg') }}" width="25"
                                                         height="25"/></a>

                                    </div>
                                    <div class="notification-message ml-3">
                                        <div>
                                            {{$notification->data['message']}}
                                        </div>

                                        <div class="notification-datetime">
                                            {{\Carbon\Carbon::parse($notification->created_at)->format('M d')}}

                                        </div>


                                    </div>


                                </div>

                            </div>

                        @empty
                            <p>You have no notifications</p>
                        @endforelse

                    </div>


                </div>


            </div>

        </div>
    </div>
@endsection
@section('style')
    <style>
        .notification-box {
            min-height: 60px;
            border-bottom: 1px solid gainsboro;
        }
    </style>
@endsection
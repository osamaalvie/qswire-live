@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-2 col-md-2 col-sm-3 col-xm-3 m-1 p-1">
                <div id="user-zones-list-container">
                    @include('zones.user_zones')
                </div>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-9 col-xm-9 m-1 p-1 mr-auto">
                @if(count($questions))
                    @foreach($questions as $question)

                        <div class="card q-{{$question->id}}" style="min-height: 75px">
                            <div class="card-body ">
                                <div class="row">
                                    <div class="col-md-11">
                                        <a class="text-secondary" href="{{route('question.select',[$question->slug])}}">
                                            <h5><b>{{$question->title}}</b></h5>
                                        </a>
                                    </div>
                                    <div class="col-md-1">
                                        @if($question->user->id == $auth->id)

                                        @endif

                                        <button class="btn btn-sm btn-danger" onclick="deleteMe(this)"
                                                data-qid="{{$question->id}}"><i
                                                    class="fa fa-trash"></i></button>

                                    </div>
                                </div>
                            </div>
                        </div>


                    @endforeach
                @else
                    <div class="card">
                        <div class="card-body" style="background-color: white">
                            <div class="no-question-asked">
                                <div class="text-center">
                                    <h6>No Question has been asked!</h6>
                                    <button class="btn btn-danger" id="btnAsk" data-toggle="modal"
                                            data-target="#questionInputModal">Ask Question
                                    </button>

                                </div>
                            </div>

                        </div>
                    </div>
                @endif

            </div>
        </div>
    </div>
@endsection
@section('style')
    <style>
        body {
            background-color: #ededed !important;
        }

        .nav-link {
            font-weight: 700 !important;
            font-size: 13.5px;
        }
    </style>
@endsection
@section('scripts')
    <script>
        function deleteMe(el) {
            let qid = $(el).data('qid');
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {

                    $.ajax({
                        url: '{{route('question.destroy')}}',
                        datatype: "json",
                        type: "POST",
                        data: {question_id: qid},
                        beforeSend: function () {
                        },
                        complete: function (response) {
                            if (response.responseJSON.status) {

                                $('.q-' + qid).remove();

                                toastr.success('Question has been deleted.');


                            } else {
                                toastr.error(response.responseJSON.message);


                            }
                        }
                    }).fail(function (jqXHR, ajaxOptions, thrownError) {
                        console.log(thrownError);

                    });

                }
            })
        }
    </script>
@endsection
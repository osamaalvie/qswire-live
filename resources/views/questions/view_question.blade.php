@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row justify-content-center">
            <div class="col-lg-8">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                <div class="card questionBox" data-id="{{$question->id}}">
                    <div class="row" style="display: flex">
                        <div class="col-md-8">
                            @foreach($question->topics as $topic)
                                <a href="#" class="topics"><span class="pb-3">{{$topic->title}}</span></a>
                            @endforeach
                        </div>


                    </div>
                    <div class="row mt-3">
                        <div class="col-md-8">
                            <h4>{{$question->title}}</h4>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-8">
                            <span>Answers: {{$question->answers()->count()}}</span>
                            <span><i class="fa fa-eye ml-4"></i>: {{$question->views()->count()}}</span>
                        </div>
                    </div>

                    <div class="row mt-3 flex-lg-row">
                        <div class="col-md-8">

                            <a class="btn btn-white btn-sm button-actions" data-toggle="collapse"
                               href="#btnAnswerEditor" role="button" aria-expanded="false"
                               aria-controls="btnAnswerEditor">
                                <img class="qBox-action-comment-count-img" src="{{ URL::asset('/images/answer.svg') }}"
                                     width="20"/>
                                Answer
                            </a>

                            <button class="btn btn-white btn-sm button-actions" onclick="followQuestion(this)"
                                    data-toggle="button" aria-pressed="{{$userQuestionFollow ? 'true': 'false'}}"
                                    data-qid="{{$question->id}}" data-uid="{{$question->user->id}}">
                                <img class="qBox-action-follow-img"
                                     src="{{ $userQuestionFollow ? URL::asset('/images/followed.png') : URL::asset('/images/unfollow.png') }}"
                                     width="20"/>
                                <span class="qBox-action-follow-title">{{$userQuestionFollow? 'Following' : 'Follow'}}</span>
                                <span class="qBox-action-follow-count"
                                      data-count="{{$question->followings()->count()}}">{{$question->followings()->count()}}</span>
                            </button>

                            <button class="btn btn-white btn-sm button-actions"
                                    data-qid="{{$question->id}}" data-uid="{{$question->user->id}}">
                                <img class="qBox-action-share-img" src="{{URL::asset('/images/share.svg')}}" width="18"
                                     alt="">
                            </button>


                            <button class="btn btn-white btn-sm button-actions" onclick="downVote(this)"
                                    data-toggle="button" aria-pressed="{{$userQuestionDownVote ? 'true': 'false'}}"
                                    data-qid="{{$question->id}}" data-uid="{{$question->user->id}}">
                                <img class="qBox-action-downvote-img"
                                     src="{{ $userQuestionDownVote ? URL::asset('/images/down-voted.png') : URL::asset('/images/down-vote.png') }}"
                                     width="20"/>
                                <span class="qBox-action-downvote-count"
                                      data-count="{{$question->downvotes()->count()}}">{{$question->downvotes()->count()}}</span>
                            </button>

                        </div>
                    </div>


                </div>
                <div id="btnAnswerEditor" class="qBox card collapse mt-2">

                    <div class="card-body">
                        <form action="{{route('answer.store')}}" method="POST" id="formAnswer"
                              data-uid="{{$auth->id}}"
                              data-qid="{{$question->id}}"
                        >
                            @csrf
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">

                                        <input type="hidden" name="question_id" value="{{$question->id}}"/>
                                        @if($answer)
                                            <input type="hidden" name="answer_id" id="answer_id"
                                                   value="{{$answer->id}}"/>
                                        @endif

                                        <textarea id="answer" name="answer" class="form-control myTextArea"></textarea>

                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-info pull-right" type="submit">Submit</button>

                                    </div>

                                </div>
                            </div>
                        </form>

                    </div>
                </div>

                @if(!$question->answers()->count())
                    <div class="no-answers" style="font-size: medium">
                        <div class="card mt-2 bg-transparent border-0">

                            <div class="card-body text-center">
                            <span class="d-block">
                                <img class="p-1 m-0" src="{{ URL::asset('/images/ask.png') }}" height="35" width="35"
                                     alt="Dev Test profile picture">

                            </span>
                                <span class="d-block">
                                {{$auth->fullName()}}
                            </span>
                                <span>Leave an answer</span>
                            </div>
                        </div>

                        <div class="card mt-2 bg-transparent border-0">

                            <div class="card-body  text-center">
                                <a class="btn btn-white btn-sm button-actions" data-toggle="collapse"
                                   href="#btnAnswerEditor" role="button" aria-expanded="false"
                                   aria-controls="btnAnswerEditor">
                                    <img class="qBox-action-comment-count-img"
                                         src="{{ URL::asset('/images/answer.svg') }}"
                                         width="20"/>
                                    Answer
                                </a>
                            </div>
                        </div>

                        <div class="card mt-2 alert alert-info">

                            <div class="card-body  text-center">
                                <span>Not answered yet,</span><br/>
                                <span>Be the first to answer!</span>
                            </div>
                        </div>
                    </div>

                @else
                    <div class="answersContainer">


                    </div>

                    @include('loadMore')
                @endif


            </div>
            <div class="col-lg-3 mr-auto">

                <div class="col-12 mb-1 adsItem bg-white">
                    <h5 class="text-center pt-2 pb-2"><i class="fa fa-bullhorn"></i> Sponsors</h5>
                </div>
                <div class="col-12 mb-1 adsItem bg-white">
                    <h5 class="text-center pt-2 pb-2">Recommended Jobs</h5>
                </div>

            </div>
        </div>
    </div>



@endsection
@section('style')
    <link href="{{ asset('css/feeds.css') }}" rel="stylesheet">

    <style type="text/css">

        .questionBox {
            width: 100%;
            padding: 10px 20px;
            color: #4e4e4e;
            border-radius: 10px !important;
        }

        .topics {
            padding: 5px 7px;
            border-radius: 50px;
            margin: 0 2px;
            color: white;
            text-decoration: none;
            background-color: #636466;
            font-size: 13px;
            font-weight: 600;
        }
        .adsItem{
            width: 100%;
            height: auto;
            margin:0 0 10px 0;
            padding: 0;
            box-shadow: 0 1px 2px rgba(48, 48, 48, 0.25);
            border-radius: 10px !important;
        }

        .adsItem h5{
            font-size: 15px;
            font-weight: 600;
            color: #4d4d4d;
        }


    </style>
@endsection
@section('scripts')
    <script src="https://cdn.tiny.cloud/1/suc5vq7xgx0mcd0dn4kh8xfyey7751n7u5afqnzmlb8p66z6/tinymce/5/tinymce.min.js"
            referrerpolicy="origin"></script>


    <script>
        var page = 1;
        let qid = 0;

        $(window).scroll(function () {
            if ($(window).scrollTop() + $(window).height() >= $(document).height()) {
                page++;
                getAnswers(qid, page);
            }
        });

        $(document).ready(function () {
            qid = '{{$question->id}}';
            getAnswers(qid, page);

            $(document).off('submit', '.formComment');
            $(document).on('submit', '.formComment', function (e) {
                e.preventDefault();
                comment(this);
            });

            $(document).off('submit', '.formReply');
            $(document).on('submit', '.formReply', function (e) {
                e.preventDefault();
                reply(this);
            });

            $('#btnAnswerEditor').on('show.bs.collapse', function () {

                if ($('#answer_id').length) {

                }
            });

            tinymce.init({
                selector: '.myTextArea',
                mode: "textareas",
                plugins: 'image code',
                toolbar: 'undo redo | link image | code',
                force_br_newlines: false,
                force_p_newlines: false,
                forced_root_block: '',
                block_unsupported_drop: false,
                file_picker_types: 'image',
                images_file_types: 'jpeg,jpg,jpe,png,bmp,gif',
                images_reuse_filename: true,
                automatic_uploads: false,
                images_upload_base_path: '{{storage_path('images')}}',
                file_picker_callback: function (callback, value, meta) {

                    // Provide image and alt text for the image dialog
                    if (meta.filetype == 'image') {
                        callback('myimage.jpg', {alt: 'My alt text'});
                    }
                }
            });
        });


        function like(e) {
            let button = $(e);
            button.prop('disabled', true);

            let uid = button.data('uid');
            let tid = button.data('tid');
            let type = button.data('type');
            let isLike = button.data('state') > 0 ? 'false' : 'true';
            let count = button.find('.count').data('count');

            $.ajax({
                url: '{{route('action.like')}}',
                datatype: "json",
                type: "POST",
                data: {user_id: uid, type_id: tid, type: type, is_like: isLike},
                beforeSend: function () {

                    button.find('.icon > i').toggleClass('blue');
                    if (isLike === 'true') {
                        count = count + 1;
                    } else {
                        if (count > 0) {
                            count = count - 1;
                        } else {
                            count = 0;
                        }
                    }

                    button.find('.count').text(count);

                }
            }).done(function (response) {
                if (response.status) {
                    button.data('state', response.data.userLikeCount);
                    button.find('.count').data('count', response.data.count);

                    button.prop('disabled', false);
                }


            }).fail(function (jqXHR, ajaxOptions, thrownError) {
                button.prop('disabled', false);
            });

        }

        function comment(e) {
            let form = $(e);
            let aid = $(form).data('aid');
            let comment = $(form).find('#comment').val();

            $.ajax({
                url: '{{route('answer.comment')}}',
                datatype: "json",
                type: "POST",
                data: {comment: comment, answer_id: aid},
                beforeSend: function () {
                    //button.prop('disabled', true);
                }
            }).done(function (response) {

                if (response.status) {
                    // button.prop('disabled', false);
                    //button.find('.question-likes-count').text(response.data.count);
                    let commentList = $('.comment_list[data-aid="' + aid + '"]');

                    commentList.prepend(response.data.html);

                    $(form).find('#comment').val('');
                    let actionPanel = $('.post-action[data-aid="' + aid + '"]');
                    actionPanel.find('.comment > .count').html(response.data.count);
                    actionPanel.find('.count').data('count', response.data.count);
                }
            }).fail(function (jqXHR, ajaxOptions, thrownError) {
                //button.prop('disabled', false);
            });
        }


        function reply(e) {
            let form = $(e);
            let cid = $(form).data('cid');
            let reply = $(form).find('.replies').val();

            $.ajax({
                url: '{{route('comment.reply')}}',
                datatype: "json",
                type: "POST",
                data: {reply: reply, comment_id: cid},
                beforeSend: function () {
                    //button.prop('disabled', true);
                }
            })
                .done(function (response) {

                    if (response.status) {
                        // button.prop('disabled', false);
                        //button.find('.question-likes-count').text(response.data.count);
                        let replyList = $('.replies_list[data-cid="' + cid + '"]');

                        replyList.prepend(response.data.html);

                        $(form).find('#reply').val('');

                        let actionPanel = $('.post-action[data-cid="' + cid + '"]');
                        actionPanel.find('.reply > .count').html(response.data.count);
                        actionPanel.find('.reply > .count').data('count', response.data.count);


                    }
                })
                .fail(function (jqXHR, ajaxOptions, thrownError) {
                    //button.prop('disabled', false);
                });
        }

        /**
         * @param qid
         * @param page
         */
        function getAnswers(qid, page) {
            $.ajax({
                url: '{{route('question.answers')}}',
                datatype: "json",
                type: "POST",
                data: {question_id: qid, page: page},
                beforeSend: function () {
                    $('.auto-load').show();

                }
            }).done(function (response) {

                if (response.count == 0) {
                    $('.auto-load').html("We don't have more data to display :(");
                    return;
                }

                $('.auto-load').hide();
                if (response.status) {
                    $('.answersContainer').append(response.view);
                }

            }).fail(function (jqXHR, ajaxOptions, thrownError) {
                $('.auto-load').hide();
            });
        }

        /**
         * @param e
         */
        function followQuestion(e) {

            let button = $(e);

            let uid = button.data('uid');
            let qid = button.data('qid');

            let src = '{{ URL::asset('/images/unfollow.png') }}';
            let following = button.attr('aria-pressed') === 'true' ? 'false' : 'true';

            $.ajax({
                url: '{{route('question.follow')}}',
                datatype: "json",
                type: "POST",
                data: {user_id: uid, question_id: qid, following: following},
                beforeSend: function () {
                    // button.prop('disabled', true);
                }
            }).done(function (response) {

                if (response.status) {
                    button.find('.qBox-action-follow-title').text(response.data.following ? 'Following' : 'Follow');

                    if (response.data.following) {
                        src = '{{ URL::asset('/images/followed.png') }}';
                    }

                    //  button.prop('disabled', false);
                    button.find('img').attr("src", src);

                    button.find('.qBox-action-follow-count').text(response.data.count);
                    button.find('.qBox-action-follow-count').data('count', response.data.count);
                }
            }).fail(function (jqXHR, ajaxOptions, thrownError) {
                //    button.prop('disabled', false);
            });
        }

        /**
         * @param e
         */
        function downVote(e) {
            let button = $(e);

            let uid = button.data('uid');
            let qid = button.data('qid');

            let src = '{{ URL::asset('/images/down-vote.png') }}';
            let downVote = button.attr('aria-pressed') === 'true' ? 'false' : 'true';


            $.ajax({
                url: '{{route('question.downvote')}}',
                datatype: "json",
                type: "POST",
                data: {user_id: uid, question_id: qid, down_vote: downVote},
                beforeSend: function () {
                    //  button.prop('disabled', true);
                }
            }).done(function (response) {

                if (response.status) {

                    if (response.data.userDownVote) {
                        src = '{{ URL::asset('/images/down-voted.png') }}';
                    }

                    //button.prop('disabled', false);

                    button.find('img').attr("src", src);
                    button.find('.qBox-action-downvote-count').text(response.data.count);
                    button.find('.qBox-action-downvote-count').data('count', response.data.count);
                }
            }).fail(function (jqXHR, ajaxOptions, thrownError) {
                // button.prop('disabled', false);
            });
        }

    </script>
@endsection

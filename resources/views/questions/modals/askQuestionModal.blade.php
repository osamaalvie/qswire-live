{{--Ask Questions Modal--}}
<div class="modal fade" id="questionInputModal" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header pt-0 pb-0">
                <ul class="nav nav-pills mb-3" id="question-tab">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="pill" href="#tabquestion" role="tab"
                           aria-selected="true">Ask Question</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="pill" role="tab" href="#tabhelp"
                           aria-selected="false">Help!</a>
                    </li>
                </ul>
            </div>
            <div class="modal-body ml-0">

                <div class="tab-content pr-3 pl-3">
                    <div class="tab-pane fade show active" id="tabquestion" role="tabpanel">
                        <div class="row">
                            <div class="col-md-6">
                                <img src="{{ URL::asset('/images/ask.png') }}" alt="Dev Test profile picture" width="25" height="25">
                                <span>Dev Test</span>
                            </div>

                        </div>
                        <div class="row mt-3">
                            <div class="col-md-12">
                                <form id="questionForm" action="{{route('question.store')}}" method="post">

                                   @csrf
                                    <div class="form-group">
                                        <label for="question">Type your question</label>
                                        <input type="text" id="question" class="form-control" autocomplete="off"
                                               autofocus="autofocus" name="question" style="padding-left: 5px" required>
                                        <small>Begin your question with 'What', 'Why', 'How', 'When', etc</small>
                                    </div>
                                    <div class="form-group">
                                        <label for="topics">Topic</label>
                                        <select id="topics" class="form-control" name="topics[]" style="width: 100%"
                                                multiple>


                                            @foreach($topics as $k => $topic)
                                                <option value="{{$k}}">{{$topic}}</option>
                                            @endforeach
                                        </select>

                                    </div>

                                    <div class="form-group pull-right">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                            Cancel
                                        </button>
                                        <button type="submit" class="btn btn-info">Add Question</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="tabhelp" role="tabpanel">Help</div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('#topics').select2({placeholder: "Select Topic",});
</script>

    <style>
        #questionInputModal ul {
            border-radius: 10px 10px 0 0 !important;
            height: 100%;
        }

        .nav-pills {
            font-weight: 400;
            color: black;
        }

        .nav-pills .nav-link.active, .nav-pills .show > .nav-link {
            color: red;
            border-bottom: 2px solid red;
            background: none;
        }

        #question {
            padding: 20px;
            padding-left: 0;
            font-size: 20px;
            /*font-family: halvetica, sans-serif;*/
            /*border-style: hidden;*/
            color: #282828;
            font-weight: 500 !important;
        }

        #questionInputModal ul {
            border-radius: 10px 10px 0 0 !important;
            height: 100%;
        }

        #tabquestion > img {
            width: 25px;
            height: 25px;
            border-radius: 100%;
        }



    </style>

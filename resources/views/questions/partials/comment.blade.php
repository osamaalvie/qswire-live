<li class="list-item">
    <div class="post">

        <div class="post-body">

            <div class="post-info">
                <a href="#"><img class="img-sm" src="{{ URL::asset('/images/avatar.jpg') }}"/></a>
                <span class="post-owner bold font-small">{{$comment->user->first_name . ' ' . $comment->user->last_name}}
                    ,</span>
                <span class="post-timestamp font-small">{{Carbon\Carbon::parse($comment->created_at)->diffForHumans()}}</span>
            </div>

            <div class="post-content mt-1">

                <span>{{$comment->content}}</span>

            </div>

        </div>

        <div class="post-action  mt-1" data-cid="{{$comment->id}}">

            <div class="p-panel-simple">

                <ul class="list-group flex-md-row">
                    <li>
                        <button class="btn btn-primary-outline like" onclick="like(this)"
                                data-tid="{{$comment->id}}"
                                data-uid="{{$comment->user->id}}"
                                data-type="comment"
                                data-state="{{$comment->userLikesCount()}}">
                            <span class="count" data-count="{{$comment->likes()->count()}}">{{$comment->likes()->count()}}</span>
                            <span class="icon"><i
                                        class="fa fa-thumbs-up icon-sm {{ $comment->userLikesCount() ? 'blue' : '' }}"></i></span>
                        </button>
                    </li>

                    <li>
                        <a class="btn btn-primary-outline reply" data-toggle="collapse"
                           href="#replySection{{$comment->id}}" role="button" aria-expanded="false"
                           aria-controls="btnAnswerEditor">
                            <span class="count" data-count="{{$comment->replies()->count()}}">{{$comment->replies()->count()}}</span>
                            <span class="icon"><i class="fa fa-reply icon-sm"></i></span>
                        </a>
                    </li>
                </ul>

            </div>

        </div>


    </div>

    <div id="replySection{{$comment->id}}" class="collapse mt-2 post-response">
        <form action="{{route('comment.reply')}}" class="formReply" method="POST"
              data-uid="{{$comment->user->id}}"
              data-cid="{{$comment->id}}">
            @csrf
            <div class="input-group">
                <a href="#">
                    <img class="p-1 m-0 img-md" src="{{ URL::asset('/images/avatar.jpg') }}"
                         alt="Dev Test profile picture"></a>
                <input id="reply" name="reply" type="text"
                       class="form-control pl-3 pr-3 replies" style="padding-left: 5px" required/>
                <div class="input-group-append">
                    <button class="btn btn-secondary btn-custom" type="submit">Reply</button>
                </div>
            </div>


        </form>

    </div>

    <ul class="list-group replies_list {{$comment->id}}" data-cid="{{$comment->id}}">

        <?php $replies = $comment->replies()->orderByDesc('created_at')->limit(3)->get(); ?>

        @foreach($replies as $reply)
            @include('questions.partials.replies',['comment'=>$comment,'reply'=>$reply])
        @endforeach
    </ul>
</li>
<div class="card post">

    <div class="card-body post-body">

        <div class="post-info">
            <a href="#"><img class="img-md" src="{{ URL::asset('/images/avatar.jpg') }}"/></a>
            <span class="post-owner bold">{{$answer->user->first_name . ' ' . $answer->user->last_name}},</span>
            <span class="post-timestamp">{{Carbon\Carbon::parse($answer->created_at)->diffForHumans()}}</span>
        </div>

        <div class="post-content mt-3">

            <span>{!! $answer->content !!}</span>
        </div>


    </div>

    <div class="post-action mt-3">

        <div class="p-panel">

            <ul class="list-group list-group-horizontal">
                <li>
                    <button class="btn btn-light like" onclick="like(this)"
                            data-tid="{{$answer->id}}"
                            data-uid="{{$answer->user->id}}"
                            data-type="answer"
                            data-state="{{$answer->userLikesCount()}}"
                    >
                        <span class="count"
                              data-count="{{$answer->likes()->count()}}">{{$answer->likes()->count()}}</span>
                        <span class="icon"><i
                                    class="fa fa-thumbs-up icon-md {{ $answer->userLikesCount() ? 'blue' : '' }}"></i></span>
                    </button>
                </li>

                <li>
                    <button class="btn btn-light comment">
                        <span class="count">{{$answer->comments()->count()}}</span>
                        <span class="icon"><i class="fa fa-comment icon-md"></i></span>
                    </button>
                </li>
            </ul>

        </div>

    </div>

    <div class="post-response mt-3 mb-3">

        <form action="{{route('answer.comment')}}" class="formComment" method="POST"
              data-aid="{{$answer->id}}"
              data-uid="{{$answer->user->id}}">
            @csrf
            <div class="input-group">
                <a href="#">
                    <img class="p-1 m-0 img-md" src="{{ URL::asset('/images/ask.png') }}"
                         alt="Dev Test profile picture"/>
                </a>
                <input id="comment" name="comment" type="text" class="form-control pl-3 pr-3" style="padding-left: 5px"
                       required/>
                <div class="input-group-append">
                    <button class="btn btn-secondary btn-custom" type="submit">Comment</button>
                </div>
            </div>

        </form>

    </div>

    <div class="post-childs mt-3 mb-3" data-id="{{$answer->id}}">
        <ul class="list-group comment_list" data-aid="{{$answer->id}}">
            @foreach($answer->comments()->orderByDesc('created_at')->limit(3)->get() as $comment)
                @include('questions.partials.comment',['comment'=>$comment])
            @endforeach
        </ul>
    </div>
</div>
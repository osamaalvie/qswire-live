<li class="list-item mt-1">
    <div class="post">

        <div class="post-body">

            <div class="post-info">
                <a href="#"><img class="img-sm" src="{{ URL::asset('/images/avatar.jpg') }}"/></a>
                <span class="post-owner bold font-small">{{$reply->user->first_name . ' ' . $reply->user->last_name}}
                    ,</span>
                <span class="post-timestamp font-small">{{Carbon\Carbon::parse($reply->created_at)->diffForHumans()}}</span>
            </div>

            <div class="post-content mt-1">

                <span>{{$reply->content}}</span>

            </div>

        </div>

        <div class="post-action">

            <div class="p-panel-simple">

                <ul class="list-group flex-md-row">
                    <li>
                        <button class="btn btn-primary-outline like" onclick="like(this)"
                                data-tid="{{$reply->id}}"
                                data-uid="{{$reply->user->id}}"
                                data-type="reply"
                                data-state="{{$reply->userLikesCount()}}">
                            <span class="count" data-count="{{$reply->likes()->count()}}">{{$reply->likes()->count()}}</span>
                            <span class="icon"><i
                                        class="fa fa-thumbs-up icon-sm {{ $reply->userLikesCount() ? 'blue' : '' }}"></i></span>
                        </button>
                    </li>

                </ul>

            </div>

        </div>


    </div>
</li>
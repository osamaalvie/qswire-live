@extends('layouts.app')

@section('content')
    <div class="container">
        {{--Ask Question Box and Feeds--}}

        <div class="row justify-content-center">
            <div class="col-lg-7 col-md-6 col-sm-9 col-xm-9 mt-1 m-1 p-1">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                <div class="col-12 mb-2">
                    <div class="card">

                        <h3></h3>

                    </div>

                </div>

            </div>
        </div>
    </div>

    {{--Ask Questions Modal--}}
    <div class="modal fade" data-backdrop="static" data-keyboard="false" id="topicSelectionModal" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header"><h3>Please select at-least one topic of your interest</h3></div>
                <div class="modal-body">
                    <form id="formTopicsSelection" action="{{route('user.topics')}}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="topics">Topic</label>
                            <select id="topics" class="form-control" name="topics[]" style="width: 100%" multiple required>


                                @foreach($topics as $k => $topic)
                                    <option value="{{$k}}">{{$topic}}</option>
                                @endforeach
                            </select>

                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary pull-right">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('style')
    <style>


    </style>
@endsection
@section('scripts')
    <script>
        $(window).on('load', function() {
            $('#topicSelectionModal').modal('show');
        });
        $(document).ready(function () {
            $('#topics').select2({placeholder: "Select Topic",});

        });
    </script>
@endsection

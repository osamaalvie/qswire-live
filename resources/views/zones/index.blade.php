@extends('layouts.app')

@section('content')
    <div class="container">
        {{--Ask Question Box and Feeds--}}

        <div class="row">
            <div class="col-md-8">
                <div class="zone-header">
                    <div class="zone-header-title">
                        <h4><b>Discover Zones</b></h4>

                    </div>
                    <div class="zone-header-text">
                        <span>Follow your favourite zones</span>
                    </div>
                    <div class="zone-header-actions">
                        <div class="flex-lg-row">
                            <button class="zone-header-button-create" type="button" data-toggle="modal"
                                    data-target="#createZoneModal">
                                <img class="p-1 m-0" src="{{ URL::asset('/images/create-zone.svg') }}" height="25"
                                     width="25" alt="Dev Test profile picture">
                                <span>Create a zone</span>
                            </button>
                            <button class="zone-header-button-discover" type="button">
                                <img class="p-1 m-0" src="{{ URL::asset('/images/discover-zone.png') }}" height="25"
                                     width="25" alt="Dev Test profile picture">
                                <span>Discover zones</span>
                            </button>
                        </div>

                    </div>
                </div>
                <div class="zone-body mt-3">
                    <div class="zone-body-grid-most-seen-container">
                        <div class="zone-body-title">
                            <h5><b>Discover Zones</b></h5>

                        </div>
                        <div class="zone-body-text">
                            <span>Most seen zones</span>

                            <div class="zone-body-most-seen-grid">

                            </div>
                        </div>


                    </div>

                    <div class="zone-body-grid-container mt-5">

                        @foreach($zoneGroups as $group)
                            <div class="zone-body-zone-category container">

                                <div class="row mb-3">
                                    <h3>{{$group['name']}}</h3>
                                </div>
                                <div class="row mb-3">
                                    @foreach($group['zones'] as $zone)
                                        <div class="col-md-4 grid-item">

                                            <div class="card"
                                                 style="border-radius: 10px; overflow: hidden; height: 275px">

                                                <div>
                                                    <a href="{{route('zone',[$zone->slug])}}" class="grid-item-link">
                                                        <div class="card-zone-header" style="min-height: 100px"
                                                             data-slug="{{$zone->slug}}">
                                                            <img class="card-zone-header-img"
                                                                 src="{{ URL::asset('/images/default-cover.jpg') }}"
                                                                 alt="Dev Test profile picture">
                                                        </div>
                                                        <div class="card-zone-middle" data-slug="{{$zone->slug}}">
                                                            <img class="card-zone-middle-img"
                                                                 src="{{ URL::asset('/images/default-avatar.jpg') }}"
                                                                 alt="Dev Test profile picture">
                                                        </div>
                                                        <div class="card-zone-body mt-2"
                                                             data-slug="{{$zone->slug}}">
                                                            <div class="card-zone-name">
                                                                <span>{{$zone->name}}</span>
                                                            </div>
                                                            <div class="card-zone-desc">
                                                                <span>{{$zone->description}}</span>
                                                            </div>

                                                        </div>

                                                    </a>
                                                </div>


                                                <div class="card-zone-footer text-center mb-3">

                                                    <?php

                                                    $userZoneFollow = $zone->isUserFollow();
                                                    ?>
                                                    <button class="btn btn-white btn-sm button-actions"
                                                            onclick="followZone(this)"
                                                            data-toggle="tooltip"
                                                            data-zid="{{$zone->id}}"
                                                            data-uid="{{$zone->user->id}}"
                                                            data-state="{{$userZoneFollow ? 'true': 'false'}}"
                                                            type="button"
                                                            title="Admin cannot unfollow the zone">
                                                        <img class="card-zone-action-follow-img"
                                                             src="{{ $userZoneFollow ? URL::asset('/images/followed.png') : URL::asset('/images/unfollow.png') }}"
                                                             width="20"/>
                                                        <span class="title">{{$userZoneFollow? 'Following' : 'Follow'}}</span>
                                                        <span class="count"
                                                              data-count="{{$zone->followings()->count()}}">{{$zone->followings()->count()}}</span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>





                                    @endforeach
                                </div>


                            </div>





                        @endforeach

                    </div>
                </div>


            </div>
            <div class="col-md-4">


            </div>
        </div>

    </div>
    @include('zones.modals.create_zone_modal')

@endsection

@section('style')
    <style>
        .zone-header {
            padding: 10px;
            background-color: white
        }

        .zone-body .zone-body-title {
        }

        .zone-header-actions button {
            font-size: 14px;
            margin-top: 10px;
            padding: 5px 10px !important;
            border-spacing: inherit;
            border-radius: 40px;
            border: 1px solid #6aa2fc;
            transition: 0.2s;
            background: none;
            color: #3498DB;
        }

        .zone-header-actions button:hover {

            background-color: #95c5ed;
            color: #1d68a7;
        }

        .grid-item .card-zone-name {
            font-size: 13px;
            font-weight: bold;
        }

        .grid-item .card-zone-desc {
            font-size: 13px;
        }

        .grid-item .card-zone-header-img {
            width: 100%;
            height: 80px;
            object-fit: cover;
            vertical-align: center;
            opacity: 0.9;
            -webkit-filter: blur(0.5px);
            filter: blur(0.5px);
            -webkit-transform: scale(1.8);
            transform: scale(1);
        }

        .grid-item .card-zone-middle-img {
            border-radius: 50%;
            position: absolute;
            width: 50px;
            height: 50px;
            object-fit: cover;
            bottom: 60%;
            left: 50%;
            max-width: 50px;
            opacity: 1;
            border: 3px solid rgba(255, 255, 255, 1);
            -webkit-transform: translate(-50%, 0%);
            transform: translate(-50%, 0%);
            z-index: 1000;

        }

        .grid-item .card-zone-body {

            text-align: center;
            height: 120px;
        }

        .grid-item .card-zone-footer {

        }

        .card-zone-footer .button-actions {
            padding: 7px 10px;
            border-radius: 50px;
            margin: 0 2px;
            background: #bdd6ff !important;
        }

        .card-zone-footer .button-actions:hover {
            color: #212529;
            text-decoration: none;
            background: #ededed;
        }

        .card-zone-footer .button-actions img {
            float: left;
            margin: 0 5px;
            object-fit: cover;
        }

        .card-zone-header:hover, .card-zone-middle:hover, .card-zone-body:hover {
            cursor: pointer;
        }

        .grid-item-link {
            color: #212529;
            text-decoration: none;
            background-color: transparent;
        }

        .grid-item-link:hover {
            text-decoration: none;
            color: #212529;
        }

        .card {
            width: 175px !important;
        }
    </style>
@endsection

@section('scripts')
    <script>
        // var page = 1;
        //
        // $(window).scroll(function () {
        //     if ($(window).scrollTop() + $(window).height() >= $(document).height()) {
        //         page++;
        //         loadMoreZones(page);
        //     }
        // });
        $(document).ready(function () {
            //loadMoreZones(page);
            $('.button-actions').hover(function () {

                $('.button-actions').tooltip('disable');

            });

        });


        {{--function loadMoreZones(page) {--}}

        {{--$.ajax({--}}
        {{--url: '{{route('zones.list')}}',--}}
        {{--datatype: "json",--}}
        {{--type: "POST",--}}
        {{--data: {page: page},--}}
        {{--beforeSend: function () {--}}
        {{--$('.auto-load').show();--}}
        {{--}--}}
        {{--})--}}
        {{--.done(function (response) {--}}
        {{--if (response.count == 0) {--}}
        {{--$('.auto-load').html("We don't have more data to display :(");--}}
        {{--return;--}}
        {{--}--}}

        {{--$('.auto-load').hide();--}}
        {{--$(".zone-body-grid-container").append(response.view);--}}

        {{--})--}}
        {{--.fail(function (jqXHR, ajaxOptions, thrownError) {--}}
        {{--console.log('Server error occured');--}}
        {{--});--}}


        {{--}--}}

        /**
         * @param e
         */
        function followZone(e) {
            let button = $(e);
            let uid = button.data('uid');
            let zid = button.data('zid');
            let auth = '{{auth()->user()->id }}';
            let isOwner = auth != uid ? 'false' : 'true';

            if (isOwner === 'true') {
                button.tooltip('enable');
                button.tooltip('show');
                button.tooltip('disable');
                return false;
            }


            let src = '{{ URL::asset('/images/unfollow.png') }}';
            let following = button.attr('data-state') === 'true' ? 'false' : 'true';
            let count = parseInt(button.find('.count').attr('data-count'));

            $.ajax({
                url: '{{route('zone.follow')}}',
                datatype: "json",
                type: "POST",
                data: {user_id: uid, zone_id: zid, following: following},
                beforeSend: function () {
                    button.prop('disabled', true);
                    button.find('.title').text(following === 'true' ? 'Following' : 'Follow');
                    button.attr('data-state', following);
                    if (following === 'true') {
                        src = '{{ URL::asset('/images/followed.png') }}';

                        count = count + 1;
                    }
                    else {
                        count = (count > 0) ? count - 1 : 0;
                    }
                    button.find('img').attr("src", src);
                    button.find('.count').html(count);
                    button.find('.count').attr('data-count', count);

                }
            }).done(function (response) {

                // if (response.status) {
                //
                // }

                button.prop('disabled', false);
            }).fail(function (jqXHR, ajaxOptions, thrownError) {
                button.prop('disabled', false);
            });
        }


    </script>
@endsection

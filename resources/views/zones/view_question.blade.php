@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row">
            <div class="col-md-8">
                <div class="card top-box">
                    <div class="card-zone-header">
                        <img class="card-zone-header-img"
                             src="{{ URL::asset('/images/default-cover.jpg') }}"
                             alt="Dev Test profile picture">
                    </div>
                    <div class="card-zone-middle" data-slug="{{$zone->slug}}">
                        <img class="card-zone-middle-img"
                             src="{{ URL::asset('/images/default-avatar.jpg') }}"
                             alt="Dev Test profile picture">
                    </div>

                    <div class="card-zone-body">
                        <div class="zone-title-box mt-5 ml-4 h-auto">
                            <span class="zone-name xl-font bold">
                                <a class="custom-link"
                                   href="{{route('zone',[$zone->slug])}}">{{$zone->name}}</a>

                            </span><br/>
                            <span class="zone-slug"><a class="custom-link"
                                                       href="{{route('zone',[$zone->slug])}}">{!! 'z/'.$zone->slug !!}</a></span><br/>
                            <span class="zone-description">{{$zone->description}}</span>
                        </div>

                        <div class="zone-action row mt-3">
                            <div class="col-md-9">
                                <?php $userZoneFollow = $zone->isUserFollow(); ?>
                                <form id="formFollow" method="POST" action="{{route('zone.follow',[$zone->id])}}">
                                    @csrf
                                    <input name="zone_id" value="{{$zone->id}}" type="hidden"/>
                                    <input name="user_id" value="{{$zone->user->id}}" type="hidden"/>
                                    <input name="following" value="{{$userZoneFollow ? 'false' : 'true'}}"
                                           type="hidden"/>
                                    <button class="btn btn-lg blue" type="submit" id="btnFollow">
                                        {{$userZoneFollow ? 'Following' : 'Follow'}} - {{$zone->followings()->count()}}                                    </button>
                                </form>
                            </div>
                            <div class="col-md-3">
                                <div class="btn-action-container">
                                    <button class="btn btn-link"><i class="fa fa-mail-forward lg-font font-black"></i>
                                    </button>
                                    <button class="btn btn-link"><i class="fa fa-ellipsis-h lg-font font-black"></i>
                                    </button>
                                </div>

                            </div>
                        </div>

                        <div class="zone-bottom-navigation-container">
                            <div class="zone-bottom-navigation">
                                <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm py-0">
                                    <div class="navbar-light col-md-12" id="navbarSupportedContent">
                                        <ul class="navbar-nav mr-auto top-bar">
                                            <li class="nav-item">
                                                <a class="nav-link {{ Request::is('z/'.$zone->slug) ? 'active' : '' }}"
                                                   href="{{route('zone',[$zone->slug])}}">Main</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link {{ Request::is('z/peoples/'.$zone->slug) ? 'active' : '' }}"
                                                   href="{{route('zone',[$zone->slug])}}">People</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link {{ Request::is('z/questions/'.$zone->slug) ? 'active' : '' }}"
                                                   href="{{route('zone',[$zone->slug])}}">Questions</a>
                                            </li>
                                        </ul>
                                    </div>
                                </nav>
                            </div>
                        </div>

                    </div>
                </div>
                @if(!$userZoneFollow)
                    <div class="row mt-3">
                        <div class="col-md-12 justify-content-center">


                            <div class="alert alert-info text-center">
                                <button type="button" class="close" data-dismiss="alert">×</button>

                                Follow <b>{{$zone->name}}</b> to publish post or ask question
                            </div>


                        </div>
                    </div>

                @endif
                <div class="card mt-3">

                    <div class="card-body">

                        <div class="q-info">
                            <a href="#"><img class="img-sm"
                                             src="{{ URL::asset('/images/avatar.jpg') }}"/></a>
                            <span class="post-owner font-small">{{$auth->fullName()}}</span>
                        </div>

                        <div class="q-content mt-3">
                            <h5 class="bold">{{$question->title}}</h5>
                        </div>

                        <div class="q-action">
                            {{$question->answers()->count()}}
                            <span>Answer</span>
                            <div class="row mt-3 flex-lg-row">
                                <div class="col-md-8">
                                    <a class="btn btn-white answer" data-toggle="collapse"
                                       href="#btnAnswerEditor{{$question->id}}" role="button"
                                       aria-expanded="false"
                                       aria-controls="btnAnswerEditor">
                                        <img class="img" src="{{ URL::asset('/images/answer.svg') }}"
                                             width="15" height="15"/>
                                        Answer
                                    </a>

                                    <button type="button"
                                            class="btn btn-white follow"
                                            data-uid="{{$zone->user->id}}"
                                            data-qid="{{$question->id}}"
                                            data-zid="{{$zone->id}}"
                                            data-state="{{$question->isUserFollow()}}"
                                            onclick="follow(this)">
                                        <img class="img"
                                             src="{{ $question->isUserFollow() ? URL::asset('/images/followed.png') : URL::asset('/images/unfollow.png') }}"
                                             width="15" height="15"/>

                                        <span class="text">{{$question->isUserFollow() ? 'Following' : 'Follow'}}</span>
                                        <span class="count"
                                              data-count="{{$question->followings()->count()}}">{{$question->followings()->count()}}</span>
                                    </button>

                                    <button class="btn btn-white share">
                                        <img class="img" src="{{ URL::asset('/images/share.svg') }}"
                                             width="15" height="15"/>

                                    </button>
                                    <button class="btn btn-white downvote">
                                        <img class="img"
                                             src="{{ URL::asset('/images/down-vote.png') }}"
                                             width="15" height="15"/>
                                    </button>


                                </div>
                            </div>
                        </div>

                        <div id="btnAnswerEditor{{$question->id}}"
                             class="card collapse mt-2 btnAnswerEditor">

                            <div class="card-body">
                                <form action="{{route('answer.store')}}" method="POST" id="formAnswer"
                                      data-uid="{{$auth->id}}"
                                      data-qid="{{$question->id}}"
                                      class="formAnswer"
                                >
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">

                                                <input type="hidden" name="question_id"
                                                       value="{{$question->id}}"/>


                                                <textarea id="answer{{$question->id}}" name="answer"
                                                          class="form-control myTextArea"></textarea>

                                            </div>
                                            <div class="form-group">
                                                <button class="btn btn-info pull-right btnEditor"
                                                        type="submit">Submit
                                                </button>

                                            </div>

                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>

                    </div>


                </div>


                @foreach($question->answers as $answer)

                    <div class="card post">

                        <div class="card-body post-body">

                            <div class="post-info">
                                <a href="#"><img class="img-md" src="{{ URL::asset('/images/avatar.jpg') }}"/></a>
                                <span class="post-owner bold">{{$answer->user->first_name . ' ' . $answer->user->last_name}}
                                    ,</span>
                                <span class="post-timestamp">{{Carbon\Carbon::parse($answer->created_at)->diffForHumans()}}</span>
                            </div>

                            <div class="post-content mt-3">

                                <span>{!! $answer->content !!}</span>
                            </div>


                        </div>

                        <div class="post-action mt-3">

                            <div class="p-panel">

                                <ul class="list-group flex-md-row">
                                    <li>
                                        <button class="btn btn-light like" onclick="like(this)"
                                                data-tid="{{$answer->id}}"
                                                data-uid="{{$answer->user->id}}"
                                                data-type="answer"
                                                data-state="{{$answer->userLikesCount()}}"
                                        >
                        <span class="count"
                              data-count="{{$answer->likes()->count()}}">{{$answer->likes()->count()}}</span>
                                            <span class="icon"><i
                                                        class="fa fa-thumbs-up icon-md {{ $answer->userLikesCount() ? 'blue' : '' }}"></i></span>
                                        </button>
                                    </li>

                                    <li>
                                        <button class="btn btn-light comment">
                                            <span class="count">{{$answer->comments()->count()}}</span>
                                            <span class="icon"><i class="fa fa-comment icon-md"></i></span>
                                        </button>
                                    </li>
                                </ul>

                            </div>

                        </div>

                        <div class="post-response mt-3 mb-3">

                            <form action="{{route('answer.comment')}}" class="formComment" method="POST"
                                  data-aid="{{$answer->id}}"
                                  data-uid="{{$answer->user->id}}">
                                @csrf
                                <div class="input-group">
                                    <a href="#">
                                        <img class="p-1 m-0 img-md" src="{{ URL::asset('/images/ask.png') }}"
                                             alt="Dev Test profile picture"/>
                                    </a>
                                    <input id="comment" name="comment" type="text" class="form-control pl-3 pr-3"
                                           style="padding-left: 5px"
                                           required/>
                                    <div class="input-group-append">
                                        <button class="btn btn-secondary btn-custom" type="submit">Comment</button>
                                    </div>
                                </div>

                            </form>

                        </div>

                        <div class="post-childs mt-3 mb-3" data-id="{{$answer->id}}">
                            <ul class="list-group comment_list" data-aid="{{$answer->id}}">
                                @foreach($answer->comments()->orderByDesc('created_at')->limit(3)->get() as $comment)
                                    @include('questions.partials.comment',['comment'=>$comment])
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @endforeach


            </div>

        </div>
    </div>
    @include('zones.modals.ask_question_modal')

@endsection

@section('scripts')
    <script src="https://cdn.tiny.cloud/1/suc5vq7xgx0mcd0dn4kh8xfyey7751n7u5afqnzmlb8p66z6/tinymce/5/tinymce.min.js"
            referrerpolicy="origin"></script>
    <script>

        $(document).ready(function () {

            tinymce.init({
                selector: '.myTextArea', mode: "textareas",
                force_br_newlines: false,
                force_p_newlines: false,
                forced_root_block: '',
            });


            $(document).on('shown.bs.collapse', function (event) {
                //console.log( "in! print e: " +event.type);
                //  event.target.scrollIntoView();
            });

            $(document).off('submit', '.formComment');
            $(document).on('submit', '.formComment', function (e) {
                e.preventDefault();
                comment(this);
            });

            $(document).off('submit', '.formReply');
            $(document).on('submit', '.formReply', function (e) {
                e.preventDefault();
                reply(this);
            });
        });


        $(".formAnswer").submit(function (e) {
            //e.preventDefault();

            let id = $(this).data('qid');

            var editorContent = tinymce.get("answer" + id).getContent();

            if (editorContent == '') {
                toastr.error('Answer cannot be empty.');
                return false;
            }

            return true;
        });

        /**
         * @param e
         */
        function follow(e) {

            let button = $(e);

            let uid = button.data('uid');
            let qid = button.data('qid');
            let zid = button.data('zid');

            let src = '{{ URL::asset('/images/unfollow.png') }}';
            let following = button.data('state') > 0 ? 'false' : 'true';
            $.ajax({
                url: '{{route('zone.question.follow')}}',
                datatype: "json",
                type: "POST",
                data: {user_id: uid, question_id: qid, zone_id: zid, following: following},
                beforeSend: function () {
                    button.prop('disabled', true);
                }
            }).done(function (response) {
                if (response.status) {
                    button.find('.text').text(response.data.following ? 'Following' : 'Follow');

                    if (response.data.following) {
                        src = '{{ URL::asset('/images/followed.png') }}';
                    }

                    button.find('img').attr("src", src);
                    button.data('state', response.data.following);
                    button.find('.count').text(response.data.count);
                    button.find('.count').data('count', response.data.count);
                }

                button.prop('disabled', false);

            }).fail(function (jqXHR, ajaxOptions, thrownError) {
                button.prop('disabled', false);
            });
        }

        /**
         * @param e
         */
        function like(e) {
            let button = $(e);
            button.prop('disabled', true);

            let uid = button.data('uid');
            let tid = button.data('tid');
            let type = button.data('type');
            let isLike = button.data('state') > 0 ? 'false' : 'true';
            let count = button.find('.count').data('count');

            $.ajax({
                url: '{{route('action.like')}}',
                datatype: "json",
                type: "POST",
                data: {user_id: uid, type_id: tid, type: type, is_like: isLike},
                beforeSend: function () {

                    button.find('.icon > i').toggleClass('blue');
                    if (isLike === 'true') {
                        count = count + 1;
                    } else {
                        if (count > 0) {
                            count = count - 1;
                        } else {
                            count = 0;
                        }
                    }

                    button.find('.count').text(count);

                }
            }).done(function (response) {
                if (response.status) {
                    button.data('state', response.data.userLikeCount);
                    button.find('.count').data('count', response.data.count);

                    button.prop('disabled', false);
                }


            }).fail(function (jqXHR, ajaxOptions, thrownError) {
                button.prop('disabled', false);
            });

        }

        function reply(e) {
            let form = $(e);
            let cid = $(form).data('cid');
            let reply = $(form).find('.replies').val();

            $.ajax({
                url: '{{route('comment.reply')}}',
                datatype: "json",
                type: "POST",
                data: {reply: reply, comment_id: cid},
                beforeSend: function () {
                    //button.prop('disabled', true);
                }
            })
                .done(function (response) {

                    if (response.status) {
                        // button.prop('disabled', false);
                        //button.find('.question-likes-count').text(response.data.count);
                        let replyList = $('.replies_list[data-cid="' + cid + '"]');

                        replyList.prepend(response.data.html);

                        $(form).find('#reply').val('');

                        let actionPanel = $('.post-action[data-cid="' + cid + '"]');
                        actionPanel.find('.reply > .count').html(response.data.count);
                        actionPanel.find('.reply > .count').data('count', response.data.count);


                    }
                })
                .fail(function (jqXHR, ajaxOptions, thrownError) {
                    //button.prop('disabled', false);
                });
        }

        function comment(e) {
            let form = $(e);
            let aid = $(form).data('aid');
            let comment = $(form).find('#comment').val();

            $.ajax({
                url: '{{route('answer.comment')}}',
                datatype: "json",
                type: "POST",
                data: {comment: comment, answer_id: aid},
                beforeSend: function () {
                    //button.prop('disabled', true);
                }
            }).done(function (response) {

                if (response.status) {
                    // button.prop('disabled', false);
                    //button.find('.question-likes-count').text(response.data.count);
                    let commentList = $('.comment_list[data-aid="' + aid + '"]');

                    commentList.prepend(response.data.html);

                    $(form).find('#comment').val('');
                    let actionPanel = $('.post-action[data-aid="' + aid + '"]');
                    actionPanel.find('.comment > .count').html(response.data.count);
                    actionPanel.find('.count').data('count', response.data.count);
                }
            }).fail(function (jqXHR, ajaxOptions, thrownError) {
                //button.prop('disabled', false);
            });
        }
    </script>
@endsection

@section('style')
    <link href="{{ asset('css/feeds.css') }}" rel="stylesheet">

    <style>
        .img-md {
            width: 40px;
            height: 40px;
            border-radius: 100%;
        }

        .top-box .card-zone-header-img {
            width: 100%;
            height: 200px;
            object-fit: cover;
            border-radius: 10px 10px 0 0;
            vertical-align: middle;
            border-style: none;
        }

        .card-zone-header {
            position: relative;
            width: 100%;
            margin: 0;
            border-radius: 10px 10px 0 0;
            box-shadow: 0px 0px 1px 0px rgb(0 0 0);
            background-color: #fff !important;
        }

        .top-box {
            border-radius: 10px 10px 0 0;
        }

        .top-box .card-zone-middle {
            border-radius: 50%;
            position: absolute;
            width: 85px;
            height: 85px;
            top: 150px;
            left: 10%;
            opacity: 1;
            border: 3px solid rgba(255, 255, 255, 1);
            -webkit-transform: translate(-50%, 0%);
            transform: translate(-50%, 0%);
            z-index: 1000;
            overflow: hidden;
        }

        .top-box .card-zone-middle img {
            position: relative;
            width: 100%;
            height: 100%;
            object-fit: cover;
        }

        .lg-font {
            font-size: large;
        }

        .xl-font {
            font-size: x-large;
        }

        .bold {
            font-weight: bold;
        }

        .custom-link {
            color: #4e4e4e;
        }

        .font-black {
            color: #4e4e4e;
        }

        .custom-link:hover {
            color: #4e4e4e;
        }

        .zone-action #formFollow {
            margin-left: 20px;
        }

        .zone-action #btnFollow {
            background-color: #007bff;
            font-size: 14px;
            font-weight: 600;
            font-family: sans-serif;
            position: relative;
            margin: 10px 0;
            width: 100%;
            border-radius: 50px;
            color: #fdfdfd;
        }

        .zone-action .btn-action-container {
            height: 25px;
            top: 25%;
            right: 0;
            position: relative;
            text-align: center;
        }

        .icon-md {
            font-size: medium;
        }

        .sm-font {
            font-size: small;
        }

        .btn-action-container button {
            border-radius: 100%;
        }

        .btn-action-container button:hover {
            border-radius: 100%;
            background-color: #ddd;
        }

        .hint button {
            background-color: #007bff;
            color: white;
        }

        .hint div {
            width: auto;
            height: 25px;
            font-size: 12px;
            color: #a9a9a9;
            display: table-cell;
            vertical-align: middle;
        }

        .img-sm {
            width: 25px;
            height: 25px;
            border-radius: 100%;
        }

        .post-action button {
            /*background-color: #cfe1ff;*/
            border: transparent;
            border-radius: 50px;
            /*color: #007bff;*/
        }

        .post-info .post-owner {
        }

        .font-small {
            font-size: 12px;
            color: #a9a9a9;
        }

        .light-grey {
            color: #a9a9a9;
        }

        .q-action button {
            font-size: small;
        }

        .blue {
            color: #007bff;
        }

        .post-response input {
            border-radius: 20px !important;
            padding: 2px 2px 2px 5px !important;
            font-size: 14px;

        }

        .post-response img {
        }

        .post-response .btn-custom {
            font-size: 12px !important;
            font-weight: 600 !important;
            background: #007bff !important;
            color: #f9f9f9 !important;
            padding: 0 10px !important;
            border-radius: 20px !important;
            margin: 0 2px !important;
            border: none !important;
        }

        .post-childs {
            padding: 5px;
        }

        .post-childs ul {
            list-style-type: none;
        }

        .post-childs .comment_list > li {
            background: #f2f2f2;
            margin-bottom: 2px;
        }

        .post-childs .comment_list > li > .post {
            padding: 3px;
        }

    </style>
@endsection
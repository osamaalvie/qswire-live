<div class="modal fade" role="dialog" tabindex="-1" id="createZoneModal">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="zone-modal-header">
                    <div class="zone-moda-title">
                        <h5 class="modal-title"><b>Create a zone</b></h5>
                    </div>
                    <div class="zone-moda-text mt-2">
                        <span>Create a zone to share your content with the world and your followers.</span>
                    </div>
                </div>

            </div>
            <div class="modal-body">
                <form action="{{route('zone.store')}}" method="POST" id="zoneCreateForm">
                    @csrf
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label for="name">Name</label>
                            <input class="form-control" type="text" name="name" id="name" required/>
                            <small>Type a unique name (max 25 chars)</small>
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="category">Category</label>
                            <select name="category" id="category" class="form-control" required>
                                <option value="">Select Category</option>
                                @foreach($categories as $k => $category)
                                    <option value="{{$k}}">{{$category}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 form-group">
                            <label for="description">Descriptions</label>
                            <textarea class="form-control" type="text" name="description" id="description" required></textarea>
                            <small>About your Zone (max 90 chars)</small>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="keywords">Keywords</label>
                                <input class="form-control" type="text" name="keywords" id="keywords"/>
                                <small>Type keywords for SEO purpose (comma separated)</small>
                            </div>

                            <div class="form-group">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-primary">Create</button>
                            </div>
                        </div>

                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<style>
    #zoneCreateForm label {
        color: #7c7c7c;
        font-weight: 600;
    }
</style>
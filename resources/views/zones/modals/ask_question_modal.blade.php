{{--Ask Questions Modal--}}
<div class="modal fade" id="questionModal" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header pt-0 pb-0">

            </div>
            <div class="modal-body ml-0">

                <div class="row">
                    <div class="col-md-6">
                        <img src="{{ URL::asset('/images/avatar.jpg') }}" alt="Dev Test profile picture" width="25" height="25">

                        <span>{{$auth->fullName()}}</span>
                    </div>

                </div>
                <div class="row mt-3">
                    <div class="col-md-12">
                        <form id="questionForm" action="{{route('zone.question.store')}}" method="post">

                           @csrf
                            <div class="form-group">
                                <label for="question">Type your question</label>
                                <input name="zone_id" type="hidden" value="{{$zone->id}}"/>
                                <input type="text" id="question" class="form-control inputQuestion" autocomplete="off"
                                       autofocus="autofocus" name="question" style="padding-left: 5px" required>
                                <small>Begin your question with 'What', 'Why', 'How', 'When', etc</small>
                            </div>

                            <div class="form-group pull-right">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                    Cancel
                                </button>
                                <button type="submit" class="btn btn-info">Post</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<style>
    #questionModal ul {
        border-radius: 10px 10px 0 0 !important;
        height: 100%;
    }

    .nav-pills {
        font-weight: 400;
        color: black;
    }

    .nav-pills .nav-link.active, .nav-pills .show > .nav-link {
        color: red;
        border-bottom: 2px solid red;
        background: none;
    }

    .inputQuestion {
        padding: 20px;
        padding-left: 0;
        font-size: 20px;
        /*font-family: halvetica, sans-serif;*/
        /*border-style: hidden;*/
        color: #282828;
        font-weight: 500 !important;
    }

    #questionModal ul {
        border-radius: 10px 10px 0 0 !important;
        height: 100%;
    }



</style>

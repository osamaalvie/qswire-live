{{--Ask Questions Modal--}}
<div class="modal fade" id="postModal" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header pt-0 pb-0">

            </div>
            <div class="modal-body ml-0">

                <div class="row">
                    <div class="col-md-6">
                        <img src="{{ URL::asset('/images/avatar.jpg') }}" alt="Dev Test profile picture" width="25"
                             height="25">
                        <span>{{$auth->fullName()}}</span>
                    </div>

                </div>
                <div class="row mt-3">
                    <div class="col-md-12">
                        <form id="postForm" action="{{route('zone.post.store',[$zone->slug])}}" method="post">

                            @csrf
                            <div class="form-group">
                                <input name="zone_id" type="hidden" value="{{$zone->id}}"/>
                                <div class="form-group">
                                    <label for="title">Post Title</label>
                                    <input type="text" id="title" class="form-control" autocomplete="off"
                                           autofocus="autofocus" name="title" style="padding-left: 5px">
                                </div>
                                <div class="form-group">
                                    <label for="post">Description</label>
                                    <textarea id="post" name="post" class="form-control myTextArea"></textarea>
                                </div>


                            </div>

                            <div class="form-group pull-right">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                    Cancel
                                </button>
                                <button type="submit" class="btn btn-info">Post</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('#topics').select2({placeholder: "Select Topic",});

    $('#postForm').submit(function (e) {
        e.preventDefault();

        if (!$(this).find('#title').val()) {
            toastr.error('Post title cannot be empty.');
            return false;
        }

        var editorContent = tinymce.get("post").getContent();

        if (!editorContent) {
            toastr.error('Post content cannot be empty.');
            return false;
        }

        this.submit();

    });
</script>

<style>
    #postModal ul {
        border-radius: 10px 10px 0 0 !important;
        height: 100%;
    }

    .nav-pills {
        font-weight: 400;
        color: black;
    }

    .nav-pills .nav-link.active, .nav-pills .show > .nav-link {
        color: red;
        border-bottom: 2px solid red;
        background: none;
    }

    .inputPost {
        padding: 20px;
        padding-left: 0;
        font-size: 20px;
        /*font-family: halvetica, sans-serif;*/
        /*border-style: hidden;*/
        color: #282828;
        font-weight: 500 !important;
    }

    #postModal ul {
        border-radius: 10px 10px 0 0 !important;
        height: 100%;
    }


</style>

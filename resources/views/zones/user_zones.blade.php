<ul class="text-secondary">

    <li class="item active">
        <a href="{{route('home')}}">
            <span><i class="fa fa-rss"></i> News Feed</span>
        </a>
    </li>

    @foreach($userZones as $zone)
        <li class="item">
            <a href="{{route('zone',[$zone->slug])}}">
                <span>
                    <img src="{{ URL::asset('/images/default-avatar.jpg') }}"
                         alt="zone picture"> {{$zone->name}}
                </span>
            </a>

        </li>
    @endforeach


</ul>


<style>
    #user-zones-list-container ul {
        margin: 0;
        padding: 0;
        list-style: none;
    }

    #user-zones-list-container ul li {
        margin: 0;
        margin-right: 5px;
        color: #4d4d4d;
        font-size: 13px;
        width: 100%;
        padding: 5px;
    }

    #user-zones-list-container ul li img {
        float: left;
        margin-right: 5px;
        width: 18px;
        height: 18px;
        object-fit: cover;
        border-radius: 3px;
    }

    #user-zones-list-container ul li i {
        margin-right: 5px;
        font-size: 18px;
    }

    #user-zones-list-container ul li a {
        text-decoration: none;
        color: #4d4d4d;
    }

    #user-zones-list-container .active {
        background: #cce4fc !important;
        color: #007bff !important;;
    }

    #user-zones-list-container .active a {
        color: #007bff !important;;
    }

    #user-zones-list-container li:hover {
        background-color: #dee2e6;
    }

    #user-zones-list-container li.active:hover {
        background-color: #dee2e6 !important;
    }
</style>
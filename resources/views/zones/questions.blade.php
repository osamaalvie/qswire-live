@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row">
            <div class="col-md-8">
                <div class="card top-box">
                    <div class="card-zone-header">
                        <img class="card-zone-header-img"
                             src="{{ URL::asset('/images/default-cover.jpg') }}"
                             alt="Dev Test profile picture">
                    </div>
                    <div class="card-zone-middle" data-slug="{{$zone->slug}}">
                        <img class="card-zone-middle-img"
                             src="{{ URL::asset('/images/default-avatar.jpg') }}"
                             alt="Dev Test profile picture">
                    </div>

                    <div class="card-zone-body">
                        <div class="zone-title-box mt-5 ml-4 h-auto">
                            <span class="zone-name xl-font bold">
                                <a style="color: black"
                                   href="{{route('zone',[$zone->slug])}}">{{$zone->name}}</a>

                            </span><br/>
                            <span class="zone-slug"><a class="custom-link"
                                                       href="{{route('zone',[$zone->slug])}}">{!! 'z/'.$zone->slug !!}</a></span><br/>
                            <span class="zone-description">{{$zone->description}}</span>
                        </div>

                        <div class="zone-action row mt-3">
                            <div class="col-md-9">
                                <?php $userZoneFollow = $zone->isUserFollow(); ?>
                                <form id="formFollow" method="POST" action="{{route('zone.follow',[$zone->id])}}">
                                    @csrf
                                    <input name="zone_id" value="{{$zone->id}}" type="hidden"/>
                                    <input name="user_id" value="{{$zone->user->id}}" type="hidden"/>
                                    <input name="following" value="{{$userZoneFollow ? 'false' : 'true'}}"
                                           type="hidden"/>
                                    <button class="btn btn-lg {{$userZoneFollow ? 'follow' : 'unfollow'}}"
                                            type="submit"
                                            id="btnFollow"
                                            data-toggle="tooltip"
                                            title="Admin cannot unfollow the zone">
                                        Follow - {{$zone->followings()->count()}}
                                    </button>
                                </form>
                            </div>
                            <div class="col-md-3">
                                <div class="btn-action-container">
                                    <button class="btn btn-link"><i class="fa fa-mail-forward lg-font font-black"></i>
                                    </button>
                                    <button class="btn btn-link"><i class="fa fa-ellipsis-h lg-font font-black"></i>
                                    </button>
                                </div>

                            </div>
                        </div>

                        <div class="zone-bottom-navigation-container">
                            <div class="zone-bottom-navigation">
                                <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm py-0">
                                    <div class="navbar-light col-md-12" id="navbarSupportedContent">
                                        <ul class="navbar-nav mr-auto top-bar">
                                            <li class="nav-item">
                                                <a class="nav-link {{ Request::is('z/'.$zone->slug) ? 'active' : '' }}"
                                                   href="{{route('zone',[$zone->slug])}}">Main</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link {{ Request::is('z/'.$zone->slug.'/members') ? 'active' : '' }}"
                                                   href="{{route('zone.members',[$zone->slug])}}">Members</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link {{ Request::is('z/'.$zone->slug.'/questions') ? 'active' : '' }}"
                                                   href="{{route('zone.questions',[$zone->slug])}}">Questions</a>
                                            </li>
                                        </ul>
                                    </div>
                                </nav>
                            </div>
                        </div>

                    </div>
                </div>
                @if(!$userZoneFollow)
                    <div class="row mt-3">
                        <div class="col-md-12 justify-content-center">


                            <div class="alert alert-info text-center">
                                <button type="button" class="close" data-dismiss="alert">×</button>

                                Follow <b>{{$zone->name}}</b> to publish post or ask question
                            </div>


                        </div>
                    </div>

                @endif
                <div class="card mt-3">
                    <div class="card-header bg-white">
                        <div class="hint">
                            <div class="icon">
                                <button class="btn-sm btn-info"><i class="fa fa-question"></i></button>
                            </div>
                            <div class="col-md-8">Question asked on {{$zone->name}}</div>
                        </div>
                    </div>
                </div>

                <div id="questionsContainer">

                </div>

                @include('loadMore')
            </div>

        </div>
    </div>
    @include('zones.modals.ask_question_modal')
    @include('zones.modals.post_modal')

@endsection

@section('scripts')

    <script>

        var page = 1;

        $(window).scroll(function () {
            if ($(window).scrollTop() + $(window).height() >= $(document).height()) {
                page++;
                loadMoreQuestions(page);
            }
        });

        function loadMoreQuestions(page) {
            let zoneID = '{{$zone->id}}';
            $.ajax({
                url: '{{route('zone.fetchQuestions',[$zone->slug])}}',
                datatype: "json",
                type: "POST",
                data: {page: page, zone_id: zoneID},
                beforeSend: function () {
                    $('.auto-load').show();
                }
            }).done(function (response) {

                if (response.count == 0) {
                    $('.auto-load').html("We don't have more data to display :(");
                    return;
                }

                $('.auto-load').hide();
                $("#questionsContainer").append(response.view);

            }).fail(function (jqXHR, ajaxOptions, thrownError) {
                console.log('Server error occured');
            });

        }

        $(function () {
            loadMoreQuestions(page);

            $('#btnFollow').hover(function () {

                $('#btnFollow').tooltip('disable');

            });


            $('#formFollow').submit(function (e) {
                e.preventDefault();
                let auth = '{{$auth->id}}';
                let uid = $(this).find('input[name="user_id"]').val();
                let isAdmin = auth == uid;

                if (isAdmin) {
                    $('#btnFollow').tooltip('enable');
                    $('#btnFollow').tooltip('show');
                } else {
                    this.submit();
                }


            });

        });

        /**
         * @param e
         */
        function follow(e) {

            let button = $(e);

            let uid = button.data('uid');
            let qid = button.data('qid');
            let zid = button.data('zid');

            let src = '{{ URL::asset('/images/unfollow.png') }}';
            let following = button.data('state') > 0 ? 'false' : 'true';
            $.ajax({
                url: '{{route('zone.question.follow')}}',
                datatype: "json",
                type: "POST",
                data: {user_id: uid, question_id: qid, zone_id: zid, following: following},
                beforeSend: function () {
                    button.prop('disabled', true);
                }
            }).done(function (response) {
                if (response.status) {
                    if (response.data.following) {
                        src = '{{ URL::asset('/images/followed.png') }}';
                    }

                    button.find('img').attr("src", src);
                    button.data('state', response.data.following);
                    button.find('.count').text(response.data.count);
                    button.find('.count').data('count', response.data.count);
                }

                button.prop('disabled', false);

            }).fail(function (jqXHR, ajaxOptions, thrownError) {
                button.prop('disabled', false);
            });
        }


    </script>
@endsection

@section('style')
    <style>

        .top-box .card-zone-header-img {
            width: 100%;
            height: 200px;
            object-fit: cover;
            border-radius: 10px 10px 0 0;
            vertical-align: middle;
            border-style: none;
        }

        .card-zone-header {
            position: relative;
            width: 100%;
            margin: 0;
            border-radius: 10px 10px 0 0;
            box-shadow: 0px 0px 1px 0px rgb(0 0 0);
            background-color: #fff !important;
        }

        .top-box {
            border-radius: 10px 10px 0 0;
        }

        .top-box .card-zone-middle {
            position: absolute;
            width: 85px;
            height: 85px;
            top: 150px;
            left: 10%;
            opacity: 1;
            transform: translate(-50%, 0%);
            z-index: 1000;
            overflow: hidden;
        }

        .top-box .card-zone-middle img {
            position: relative;
            width: 100%;
            height: 100%;
            object-fit: cover;
            border-radius: 100%;
            border-color: white;
            background: white;
            padding: 4px;
        }

        .lg-font {
            font-size: large;
        }

        .xl-font {
            font-size: x-large;
        }

        .bold {
            font-weight: bold;
        }

        .custom-link {
            color: #4e4e4e;
        }

        .font-black {
            color: #4e4e4e;
        }

        .custom-link:hover {
            color: #4e4e4e;
        }

        .zone-action #formFollow {
            margin-left: 20px;
        }

        .zone-action #btnFollow {

        }

        .unfollow {
            background-color: #007bff;
            font-size: 14px;
            font-weight: 600;
            font-family: sans-serif;
            position: relative;
            margin: 10px 0;
            width: 100%;
            border-radius: 50px;
            border-color: white;
            color: white;
        }

        .unfollow:hover {
            background-color: white;
            border-color: #007bff;
            color: #007bff;
        }

        .follow:hover {

            background-color: #007bff;
            border-radius: 50px;
            color: white !important;
            border-color:  #007bff;

        }

        .follow {
            font-size: 14px;
            font-weight: 600;
            font-family: sans-serif;
            position: relative;
            margin: 10px 0;
            border-radius: 50px;
            color: #007bff !important;
        }

        .zone-action .btn-action-container {
            height: 25px;
            top: 25%;
            right: 0;
            position: relative;
            text-align: center;
        }

        .icon-md {
            font-size: medium;
        }

        .sm-font {
            font-size: small;
        }

        .btn-action-container button {
            border-radius: 100%;
        }

        .btn-action-container button:hover {
            border-radius: 100%;
            background-color: #ddd;
        }

        .hint button {
            background-color: #007bff;
            color: white;
        }

        .hint div {
            width: auto;
            height: 25px;
            font-size: 12px;
            color: #a9a9a9;
            display: table-cell;
            vertical-align: middle;
        }

        .img-sm {
            width: 25px;
            height: 25px;
            border-radius: 100%;
        }

        .post-action button {
            background-color: #cfe1ff;
            border: transparent;
            border-radius: 50px;
            color: #007bff;
        }

        .post-info .post-owner {
        }

        .font-small {
            font-size: 12px;
            color: #a9a9a9;
        }

        .light-grey {
            color: #a9a9a9;
        }

        .q-action button {
            font-size: small;
        }

        .link-no-style:hover {
            color: #4e4e4e;
        }

    </style>
@endsection
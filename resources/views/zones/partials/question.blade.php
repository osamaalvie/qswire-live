@foreach($questions as $question)
    <div class="card">
        <div class="card-body">
            <?php $userQuestionFollow = $question->userFollow->count() > 0; ?>
            <div class="q-info">
                <a href="#"><img class="img-sm"
                                 src="{{ URL::asset('/images/avatar.jpg') }}"/></a>
                <span class="post-owner font-small">{{$auth->fullName()}}</span>
            </div>

            <div class="q-content mt-3">
                <a href="{{route('zone.question',['zone'=>$zone->slug, 'question'=>$question->slug])}}"
                   class="link-no-style font-black">
                    <h5 class="bold">{{$question->title}}</h5>
                </a>
            </div>

            <div class="q-action">
                <a class="link-no-style font-black"
                   href="{{route('zone.question',['zone'=>$zone->slug, 'question'=>$question->slug])}}">{{$question->answers()->count()}}
                    <span>Answer</span></a>
                <div class="row mt-3 flex-lg-row">
                    <div class="col-md-8">
                        <a class="btn btn-white answer" data-toggle="collapse"
                           href="#btnAnswerEditor{{$question->id}}" role="button"
                           aria-expanded="false"
                           aria-controls="btnAnswerEditor">
                            <img class="img" src="{{ URL::asset('/images/answer.svg') }}"
                                 width="15" height="15"/>
                            Answer
                        </a>

                        <button type="button"
                                class="btn btn-white"
                                data-uid="{{$zone->user->id}}"
                                data-qid="{{$question->id}}"
                                data-zid="{{$zone->id}}"
                                data-state="{{$userQuestionFollow}}"
                                onclick="follow(this)">
                            <img class="img"
                                 src="{{ $userQuestionFollow ? URL::asset('/images/followed.png') : URL::asset('/images/unfollow.png') }}"
                                 width="15" height="15"/>

                            <span class="text">{{$userQuestionFollow ? 'Following' : 'Follow'}}</span>
                            <span class="count"
                                  data-count="{{$question->followings()->count()}}">{{$question->followings()->count()}}</span>
                        </button>

                        <button class="btn btn-white share">
                            <img class="img" src="{{ URL::asset('/images/share.svg') }}"
                                 width="15" height="15"/>

                        </button>
                        <button class="btn btn-white downvote">
                            <img class="img"
                                 src="{{ URL::asset('/images/down-vote.png') }}"
                                 width="15" height="15"/>
                        </button>


                    </div>
                </div>
            </div>

            <div id="btnAnswerEditor{{$question->id}}"
                 class="card collapse mt-2 btnAnswerEditor">

                <div class="card-body">
                    <form action="{{route('answer.store')}}" method="POST" id="formAnswer"
                          data-uid="{{$auth->id}}"
                          data-qid="{{$question->id}}"
                          class="formAnswer"
                    >
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    @csrf
                                    <input type="hidden" name="question_id"
                                           value="{{$question->id}}"/>


                                    <textarea id="answer{{$question->id}}" name="answer"
                                              class="form-control myTextArea"></textarea>

                                </div>
                                <div class="form-group">
                                    <button class="btn btn-info pull-right btnEditor"
                                            type="submit">Submit
                                    </button>

                                </div>

                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>


    </div>

@endforeach